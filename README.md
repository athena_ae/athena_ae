<div><p align="center"><a href="https://gitlab.com/athena_ae/athena_ae/wikis/home"><img src="https://gitlab.com/athena_ae/athena_ae/wikis/uploads/ff3a64112d87a468bd500451f16af842/ATHENA_AE-logo.png" alt="ATHENA_AE-logo" height="300" align="center"></a></p></div>

# *Athena &AElig; v1.0* &mdash; *Athena* Atmospheric Escape (&AElig;)
## Unoffical branch of *Athena* v4.2 for atmospheric escape
### John McCann UCSB ([mccann@ucsb.edu](mailto:mccann@ucsb.edu))

Current release summary
-----------------------

The v1.0 release is the first actual public release of *Athena &AElig;*! Now capable of 3-D radiative-hydrodynamic simulations of tidally-locked hydrogen atmospheres receiving large amounts of ionizing XUV flux in various stellar environments for the low planetary magnetic field case. Users can explore the various effects of orbital and stellar wind effects. This code should be capable of fully reproducing all results presented in McCann et al. ([2018](https://arxiv.org/abs/1811.09276v1)). One should also checkout our post-processing tools ([pp\_tools](https://gitlab.com/athena_ae/pp_tools)) to produce figures (included via a [slymodule](https://gitlab.com/JohnRyan/_slymodule)).

**Looking for [movies](https://gitlab.com/athena_ae/athena_ae/wikis/Papers/McCann2018/Movies) from a publication?** Please visit our [wiki](https://gitlab.com/athena_ae/athena_ae/wikis/home) for more information, e.g., [McCann et al. 2018](https://gitlab.com/athena_ae/athena_ae/wikis/Papers/McCann2018).

How to build and run *Athena &AElig;*
-------------------------------------

Before your first execution of program, you must run GNU's `autoconf` from the top directory (*athena\_ae/*) to generate the configure file compatiable with the machine's unquie architecture. Afterwards you should have a configure executable with which you will provide the [physics and alogrithim, options and feature](https://trac.princeton.edu/Athena/wiki/AthenaDocsUGConfigure) input relevant to your problem. The following example will configure your build to reproduce the results from McCann et al. ([2018]()).

Example:
````bash
./configure --with-problem=planet_ae --with-gas=hydro --enable-ion-radiation --enable-ion-plane --with-flux=roe --enable-h-correction --enable-fofc --with-nscalars=1 --enable-mpi --enable-smr --enable-shearing-box
````

Now your *Makefile* is configured you can finally build *Athena &AElig;* with

````bash
make all
````

Finally you ready to run *Athena &AElig;*. To execute, give the code an *athinput* file, found in *athena\_ae/tst/*. Remember one can run `bin/athena -h` after the code is compiled to see the general *Athena* help guide. The following provided exmaple is our intermediate stellar wind from McCann et al. ([2018]()) in a full rotating frame. We assume there are 115 cores avaible to run the simulation on, as simulations such as these are impratical to run on a single desktop. The outputs will be written to the folder *athena\_ae/sim/Rotating\_15e3/*.

````bash
mpirun -n 115 bin/athena -d sim/Rotating_15e3 -i tst/planet/athinput.planet_ae_large
````

Incorporating slymodules
--------------------------

To keep the project as trim as possible, superfluous (but useful) secondary tools have been kept seperate. Things such as a python wrapper for *Athena &AElig;* ([shim_tools](https://gitlab.com/athena_ae/shim_tools)), our post-processing pipeline ([pp_tools](https://gitlab.com/athena_ae/pp_tools)), and our development standardization tools ([dev_tools](https://gitlab.com/athena_ae/dev_tools)) have been included in the project as [slymodules](https://gitlab.com/JohnRyan/_slymodule). These are like submodules but more sly, see the git repo for more information. To grab all of these tools run the following

````bash
git checkout _slymodules
git submodule init; git submodule update
git checkout master
````

You should now all the above tools added to your repository; however, these are not tracked as part of the *Athena &AElig;* project by design! To learn more about each tool visit the above corrisponding linked git repo.

Adding your own physics
-----------------------

To solve your own problems, I recommend first making your own athinput files to set your problem's numerical scale. The athinput file is mostly used in conjunction with the problem file, which set the intial grid conditions. You should therefore also create a problem file to setup your problem. Lastly, you might be unfortunate enough to find that *Athena &AElig;* doesn't incorperate the physics you desire. In that case you should consider becoming an *Athena &AElig;* contributor and modifying the source code yourself!

Contributing to *Athena &AElig;*
--------------------------------

Meticulous effort has been put in establishing an ecosystem in which anyone can contribute to the project. If you wish to contribute to the project you should checkout the *dev* branch. Here you will be provided with a folder which contains the necessary git hooks and code beautifier scripts ([*uncrustify*](https://github.com/uncrustify/uncrustify) is required) to help keep the project uniform in design. The project follows the [*gitflow*](https://nvie.com/posts/a-successful-git-branching-model/) work flow for branching, and as such we ask that you `--no-ff` your merges of features into the dev branch. You will also be asked to following the formatting procedure, made possible by [*uncrustify*](https://github.com/uncrustify/uncrustify). Lastly, commit messages should adhear to guidelines lain out by [Chris Beams](http://chris.beams.io/posts/git-commit/).

For simplicity you can run

````bash
./dev/setup.sh
````

to configure your git to be a complient contributor. As a security measure it is recommended you look over this script before running as it alters your git configure&mdash;but only local to the *Athena &AElig;* project.
