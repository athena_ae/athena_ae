#include "../copyright.h"
/*==============================================================================
 * FILE: ionradplane_3d.c
 *
 * PURPOSE: Contains functions to compute an ionization radiative transfer
 *   from point sources update, using the algorithm described in
 *   Krumholz, Stone, & Gardiner (2007), or transfer from a
 *   plane-parallel radiation front.
 *
 *   Use of these routines requires that --enable-ion-radiation be set
 *   at compile time.
 *
 * CONTAINS PUBLIC FUNCTIONS:
 *   add_radplane_3d             - adds a new radiation source
 *   ion_radplane_init_3d        - handles internal initialization
 *   get_ph_rate_plane           - computed photoionzation rate from
 *                                    a planar source
 *============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include "../defs.h"
#include "../athena.h"
#include "prototypes.h"
#include "../prototypes.h"
#include "../globals.h"
#include "ionrad.h"

#ifdef ION_RADPLANE
Real erfinv(Real y);
void flux_ramp(DomainS* pD, Radplane *plane, Real t);
void init_framp();

/* Initialized number of radiation planes to zero */
void ion_radplane_init_3d(MeshS *pM) {
  int nl, nd;

  for (nl=0; nl<pM->NLevels; nl++){
    for (nd=0; nd<pM->DomainsPerLevel[nl]; nd++){
      if (pM->Domain[nl][nd].Grid != NULL) {
        pM->Domain[nl][nd].Grid->nradplane = 0;
      }
    }
  }

  init_framp();

  return;
}

/* --------------------------------------------------------------
 * Routine to add new radiator plane
 * --------------------------------------------------------------
 */

void add_radplane_3d(DomainS *pD, int dir, Real flux) {
  GridS *pG = pD->Grid;
  int n, n1z, n2z, n3z;
  int i, is, ie, j, js, je, k, ks, ke;

  /* Add radiator to pgrid structure */
  pG->nradplane++;
  if (pG->nradplane==1) {
    if (!(pG->radplanelist = malloc(sizeof(Radplane)))) {
      goto on_error;
    }
  }
  else {
    if (!(pG->radplanelist = realloc(pG->radplanelist,
					pG->nradplane*sizeof(Radplane)))) {
      goto on_error;
    }
  }
  n = pG->nradplane-1;
  pG->radplanelist[n].dir = dir;
  pG->radplanelist[n].flux = flux;
  /* Allocate cell fluxes */
  n1z = pG->Nx[0]+2*nghost;
  n2z = pG->Nx[1]+2*nghost;
  n3z = pG->Nx[2]+2*nghost;
  pG->radplanelist[n].CFlux = (Real***)calloc_3d_array(n3z, n2z, n1z, sizeof(Real));
  if (pG->radplanelist[n].CFlux == NULL) {
    goto on_error;
  }

  flux_ramp(pD, &(pG->radplanelist[n]), pG->time);

  return;

  on_error:
    ath_error("[add_radplane_3d]: malloc returned a NULL pointer\n");
}

  /* Set boundary fluxes only if grid touches physical boundary */
/*  if ( (dir == -1 && pG->MinX[0] != pD->RootMinX[0]) ||
       (dir ==  1 && pG->MaxX[0] != pD->RootMaxX[0]) ||
       (dir == -2 && pG->MinX[1] != pD->RootMinX[1]) ||
       (dir ==  2 && pG->MaxX[1] != pD->RootMaxX[1]) ||
       (dir == -3 && pG->MinX[2] != pD->RootMinX[2]) ||
       (dir ==  3 && pG->MaxX[2] != pD->RootMaxX[2]) ) {
    return;
  }
*/  /* Set bounds depending on direction */
/*  if (dir == 1 || dir == -1) {
    is = dir < 0 ? 0 : pG->ie+nghost; js = 0; ks = 0;
    ie = nghost; je = n2z; ke = n3z;
  }
  else if (dir == 2 || dir == -2) {
    is = 0; js = dir < 0 ? 0 : pG->je+nghost; ks = 0;
    ie = n1z; je = nghost; ke = n3z;
  }
  else if (dir == 3 || dir == -3) {
    is = 0; js = 0; ks = dir < 0 ? 0 : pG->ke+nghost;
    ie = n1z; je = n2z; ke = nghost;
  }
  else{
    ath_error("[add_radplane_3d]: dir=%d, can only be +/- 1, 2, 3\n", dir);
  }


*/  /* Set flux in boundary */
/*  for (k=ks; k<ke; k++) {
    for (j=js; j<je; j++) {
      for (i=is; i<ie; i++) {
        pG->radplanelist[n].CFlux[k][j][i] = flux;
      }
    }
  }

  return;

  on_error:
    ath_error("[add_radplane_3d]: malloc returned a NULL pointer\n");
}
*/

/* --------------------------------------------------------------
 * Routine to compute photoionization rate from a plane radiation
 * source.
 * --------------------------------------------------------------
 */

void get_ph_rate_plane(Radplane *plane, Real ***ph_rate, DomainS *pD) {
  GridS *pG = pD->Grid;
  int lr, s;
  Real tau, n_H, kph, etau, cell_len;
  Real flux, flux_frac;
  int i, ii, j, jj, k, kk;
#ifdef MPI_PARALLEL
  int n, nGrid = 0;
  int myrank, nextproc, prevproc, err;
  int planesize;
  Real *planeflux = NULL;
  Real max_flux_frac, max_flux_frac_glob;
  MPI_Status stat;
#endif
  int dir = plane->dir;

  /* Set lr based on whether radiation is left or right
     propagating. lr = 1 is for radiation going left to right. Also
     set up the start and end indices based on the direction, and
     store the cell length. */
  lr = (dir < 0) ? 1 : -1;
  switch(dir) {
  case -1: case 1: {
    if (lr > 0) {
      s=pG->is;
    }
    else {
      s=pG->ie;
    }
    cell_len = pG->dx1;
    break;
  }
  case -2: case 2: {
    if (lr > 0) {
      s=pG->js;
    }
    else {
      s=pG->je;
    }
    cell_len = pG->dx2;
    break;
  }
  case -3: case 3: {
    if (lr > 0) {
      s=pG->ks;
    }
    else {
      s=pG->ke;
    }
    cell_len = pG->dx3;
    break;
  }
  }

#ifdef MPI_PARALLEL
  /* Figure out processor geometry: where am I, where are my neighbors
     upstream and downstream, how many processors are there in the
     direction of radiation propagation, how big is the interface with
     my neighbor? */
  for (k=0; k<pD->NGrid[2]; k++) {
    for (j=0; j<pD->NGrid[1]; j++) {
      for (i=0; i<pD->NGrid[0]; i++) {
	if (pD->GData[k][j][i].ID_Comm_world == myID_Comm_world) {
	  switch(dir) {
	  case -1: case 1: {
	    nGrid=pD->NGrid[0];
	    myrank = lr > 0 ? i : nGrid - i - 1;
	    planesize=pG->Nx[1]*pG->Nx[2];
	    if ((i-lr >= 0) && (i-lr <= pD->NGrid[0]-1))
	      prevproc = pD->GData[k][j][i-lr].ID_Comm_world;
	    else
	      prevproc = -1;
	    if ((i+lr >= 0) && (i+lr <= pD->NGrid[0]-1))
	      nextproc = pD->GData[k][j][i+lr].ID_Comm_world;
	    else
	      nextproc = -1;
	    break;
	  }
	  case -2: case 2: {
	    nGrid=pD->NGrid[1];
	    myrank = lr > 0 ? j : nGrid - j - 1;
	    planesize=pG->Nx[0]*pG->Nx[2];
	    if ((j-lr >= 0) && (j-lr <= pD->NGrid[1]-1))
	      prevproc = pD->GData[k][j-lr][i].ID_Comm_world;
	    else
	      prevproc = -1;
	    if ((j+lr >= 0) && (j+lr <= pD->NGrid[1]-1))
	      nextproc = pD->GData[k][j+lr][i].ID_Comm_world;
	    else
	      nextproc = -1;
	    break;
	  }
	  case -3: case 3: {
	    nGrid=pD->NGrid[2];
	    myrank = lr > 0 ? k : nGrid - k - 1;
	    planesize=pG->Nx[0]*pG->Nx[1];
	    if ((k-lr >= 0) && (k-lr <= pD->NGrid[2]-1))
	      prevproc = pD->GData[k-lr][j][i].ID_Comm_world;
	    else
	      prevproc = -1;
	    if ((k+lr >= 0) && (k+lr <= pD->NGrid[1]-1))
	      nextproc = pD->GData[k+lr][j][i].ID_Comm_world;
	    else
	      nextproc = -1;
	    break;
	  }
	  default:
	    ath_error("[get_ph_rate_plane]: dir must be +-1, 2, or 3\n");
	  }
	}
      }
      if (nGrid != 0) {
        break;
      }
    }
    if (nGrid != 0) {
      break;
    }
  }

  /* Allocate memory for flux at interface on first pass */
  if (!(planeflux=calloc(planesize, sizeof(Real)))) {
    ath_error("[get_ph_rate_plane]: calloc returned a null pointer!\n");
  }

  /* Loop over processors in the direction of propagation */
  for (n=0; n<nGrid; n++) {

    /* Set to 0 flux fraction remaining to start */
    max_flux_frac = 0.0;

    /* Am I the rank before the current one? If so, pass the flux on
       to the next processor. */
    if (myrank == n-1) {
      err = MPI_Send(planeflux, planesize, MP_RL, nextproc, n, MPI_COMM_WORLD);
      if (err) {
        ath_error("[get_ph_rate_plane]: MPI_Send error = %d\n", err);
      }
    }

    /* Is it my turn to compute the transfer now? */
    if (myrank == n) {
      /* If not the first processor, get the flux from the previous and set the
         flux at edge, else already set from smr or initalization */
      if (prevproc != -1) {
        err = MPI_Recv(planeflux, planesize, MP_RL, prevproc, n, MPI_COMM_WORLD, &stat);
        if (err){
          ath_error("[get_ph_rate_plane]: MPI_Send error = %d\n", err);
        }
        switch(dir) {
          case -1: case 1: {
            for (k=pG->ks; k<=pG->ke; k++) {
              for (j=pG->js; j<=pG->je; j++) {
                for (i=0; i<nghost; i++) {
                  plane->CFlux[k][j][s-lr*(1+i)] = planeflux[(k-pG->ks)*pG->Nx[1]+j-pG->js];
                }
              }
            }
          }
          case -2: case 2: {
            for (k=pG->ks; k<=pG->ke; k++) {
              for (j=0; j<nghost; j++) {
                for (i=pG->is; i<=pG->ie; i++) {
                  plane->CFlux[k][s-lr*(1+j)][i] = planeflux[(k-pG->ks)*pG->Nx[0]+i-pG->is];
                }
              }
            }
          }
          case -3: case 3: {
            for (k=0; k<nghost; k++) {
              for (j=pG->js; j<=pG->je; j++) {
                for (i=pG->is; i<=pG->ie; i++) {
                  plane->CFlux[s-lr*(1+k)][j][i] = planeflux[(j-pG->js)*pG->Nx[0]+i-pG->is];
                }
              }
            }
          }
        }
      }
#endif /* MPI_PARALLEL */

      /* Alter flux by ramping factor, only for physical boundaries */
      flux_ramp(pD, plane, pG->time);

      /* Propograte the radiation */
      switch(dir) {
      case -1: case 1: {
	for (k=pG->ks; k<=pG->ke; k++) {
	  for (j=pG->js; j<=pG->je; j++) {
            flux = plane->CFlux[k][j][s-lr];
	    for (i=s, ii=0; ii<pG->Nx[0]; i+=lr, ii++) {
	      n_H = pG->U[k][j][i].s[0] / m_H;
	      tau = sigma_ph * n_H * pG->dx1;
	      etau = exp(-tau);
	      kph = flux * (1.0-etau) / (n_H*cell_len);
	      ph_rate[k][j][i] += kph;
	      flux *= etau;
	      flux_frac = flux / plane->CFlux[k][j][s-lr];
	      if (flux_frac < MINFLUXFRAC) {
                break;
              }
              plane->CFlux[k][j][i] = flux;
	    }
#ifdef MPI_PARALLEL
	    /* Store final flux to pass to next processor, or 0 if we
	       ended the loop early because we were below the minimum
	       fraction. */
	    planeflux[(k-pG->ks)*pG->Nx[1]+j-pG->js] =
	      flux_frac < MINFLUXFRAC ? 0.0 : flux;
	    max_flux_frac = (flux_frac > max_flux_frac) ?
	      flux_frac : max_flux_frac;
#endif /* MPI_PARALLEL */
	  }
	}
//ath_pout(-1,"fin radtrans:%d\n", myID_Comm_world);
	break;
      }
      case -2: case 2: {
	for (k=pG->ks; k<=pG->ke; k++) {
	  for (i=pG->is; i<=pG->ie; i++) {
            flux = plane->CFlux[k][s-lr][i];
	    for (j=s, jj=0; jj<pG->Nx[1]; j+=lr, jj++) {
	      n_H = pG->U[k][j][i].s[0] / m_H;
	      tau = sigma_ph * n_H * pG->dx1;
	      etau = exp(-tau);
	      kph = flux * (1.0-etau) / (n_H*cell_len);
	      ph_rate[k][j][i] += kph;
	      flux *= etau;
	      flux_frac = flux / plane->CFlux[k][s-lr][i];
	      if (flux_frac < MINFLUXFRAC) {
                break;
              }
              plane->CFlux[k][j][i] = flux;
	    }
#ifdef MPI_PARALLEL
	    /* Store final flux to pass to next processor */
	    planeflux[(k-pG->ks)*pG->Nx[0]+i-pG->is] =
	      flux_frac < MINFLUXFRAC ? 0.0 : flux;
	    max_flux_frac = (flux_frac > max_flux_frac) ?
	      flux_frac : max_flux_frac;
#endif /* MPI_PARALLEL */
	  }
	}
	break;
      }
      case -3: case 3: {
	for (j=pG->js; j<=pG->je; j++) {
	  for (i=pG->is; i<=pG->ie; i++) {
            flux = plane->CFlux[s-lr][j][i];
	    for (k=s, kk=0; kk<pG->Nx[2]; k+=lr, kk++) {
	      n_H = pG->U[k][j][i].s[0] / m_H;
	      tau = sigma_ph * n_H * pG->dx1;
	      etau = exp(-tau);
	      kph = flux * (1.0-etau) / (n_H*cell_len);
	      ph_rate[k][j][i] += kph;
	      flux *= etau;
	      flux_frac = flux / plane->CFlux[s-lr][j][i];
	      if (flux_frac < MINFLUXFRAC) {
                break;
              }
              plane->CFlux[k][j][i] = flux;
	    }
#ifdef MPI_PARALLEL
	    /* Store final flux to pass to next processor */
	    planeflux[(j-pG->js)*pG->Nx[0]+i-pG->is] =
	      flux_frac < MINFLUXFRAC ? 0.0 : flux;
	    max_flux_frac = (flux_frac > max_flux_frac) ?
	      flux_frac : max_flux_frac;
#endif /* MPI_PARALLEL */
	  }
	}
	break;
      }
      }

#ifdef MPI_PARALLEL
    }

    /* If we're parallel, get the maximum flux fraction left and see
       if we should continue to the next set of processors. */
    err = MPI_Allreduce(&max_flux_frac, &max_flux_frac_glob, 1, MP_RL,
			MPI_MAX, pD->Comm_Domain);
    if (err) {
      ath_error("[get_ph_rate_plane]: MPI_Allreduce error = %d\n", err);
    }
    if (max_flux_frac_glob < MINFLUXFRAC) {
      break;
    }
  }

  free(planeflux);
#endif /* MPI_PARALLEL */

  return;
}

#define CENTRAL_RANGE 0.7
Real erfinv(Real y) {
  Real x,z,num,dem;
  /* coefficients in rational expansion */
  Real a[4]={ 0.886226899, -1.645349621,  0.914624893, -0.140543331};
  Real b[4]={-2.118377725,  1.442710462, -0.329097515,  0.012229801};
  Real c[4]={-1.970840454, -1.624906493,  3.429567803,  1.641345311};
  Real d[2]={ 3.543889200,  1.637067800};

  if(fabs(y) > 1.0) {
    return atof("NaN");
  }
  if(fabs(y) == 1.0) {
    return copysign(LARGE, y);
  }
  if(fabs(y) <= CENTRAL_RANGE ) {
  z = y*y;
  num = ((a[3]*z + a[2])*z + a[1])*z + a[0];
  dem = (((b[3]*z + b[2])*z + b[1])*z +b[0])*z + 1.0;
  x = y*num/dem;
  }
  else if(fabs(y) > CENTRAL_RANGE && fabs(y) < 1.0) {
    z = sqrt(-log((1.0-fabs(y))/2.0));
    num = ((c[3]*z + c[2])*z + c[1])*z + c[0];
    dem = (d[1]*z + d[0])*z + 1.0;
    x = copysign(num/dem, y);
  }
  /* Two steps of Newton-Raphson correction */
  x = x-(erf(x)-y)/(2.0/sqrt(M_PI)*exp(-x*x));
  x = x-(erf(x)-y)/(2.0/sqrt(M_PI)*exp(-x*x));

  return x ;
}

static Real t_half, f_0, erfi, t_end;
static int ramp_done = 0;

void init_framp(){
  t_half = 5.e4;
  f_0 = 0.01;
  erfi = erfinv(1.-2.*f_0);
  Real tol = 0.001;
  t_end = t_half*(erfinv(1.-2.*tol)/erfi+1);

  return;
}

void flux_ramp(DomainS *pD, Radplane *plane, Real t){
  GridS *pG = pD->Grid;
  int i, is, ie, j, js, je, k, ks, ke;
  int n1z, n2z, n3z, dir = plane->dir;
  Real ramp;

  /* Check if we need to ramp anymore */
  if (ramp_done) {
    return;
  }

  /* All grids record ramp */
  if (t < t_end) {
    ramp = 0.5*(1.+erf(erfi*(t/t_half-1.)));
  }
  else {
    ramp = 1.0;
    ramp_done = 1;
  }
  plane->ramp = ramp;

  /* Ramp boundary fluxes only if grid touches physical boundary */
  if ( (dir == -1 && pG->MinX[0] != pD->RootMinX[0]) ||
       (dir ==  1 && pG->MaxX[0] != pD->RootMaxX[0]) ||
       (dir == -2 && pG->MinX[1] != pD->RootMinX[1]) ||
       (dir ==  2 && pG->MaxX[1] != pD->RootMaxX[1]) ||
       (dir == -3 && pG->MinX[2] != pD->RootMinX[2]) ||
       (dir ==  3 && pG->MaxX[2] != pD->RootMaxX[2]) ) {
    return;
  }

  /* Allocate cell fluxes */
  n1z = pG->Nx[0]+2*nghost;
  n2z = pG->Nx[1]+2*nghost;
  n3z = pG->Nx[2]+2*nghost;
  /* Set bounds depending on direction */
  if (dir == 1 || dir == -1) {
    is = dir < 0 ? 0 : pG->ie+nghost; js = 0; ks = 0;
    ie = nghost; je = n2z; ke = n3z;
  }
  else if (dir == 2 || dir == -2) {
    is = 0; js = dir < 0 ? 0 : pG->je+nghost; ks = 0;
    ie = n1z; je = nghost; ke = n3z;
  }
  else if (dir == 3 || dir == -3) {
    is = 0; js = 0; ks = dir < 0 ? 0 : pG->ke+nghost;
    ie = n1z; je = n2z; ke = nghost;
  }
  else{
    ath_error("[flux_ramp]: dir=%d, can only be +/- 1, 2, 3\n", dir);
  }

  /* Set flux in boundary */
  for (k=ks; k<ke; k++) {
    for (j=js; j<je; j++) {
      for (i=is; i<ie; i++) {
        plane->CFlux[k][j][i] = plane->flux*ramp;
      }
    }
  }

  return;
}

#endif /* ION_RADPLANE */
