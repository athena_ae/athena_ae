#ifndef IONRAD_PROTOTYPES_H
#define IONRAD_PROTOTYPES_H
#include "../copyright.h"
/*==============================================================================
 * FILE: prototypes.h
 *
 * PURPOSE: Prototypes for all public functions from the following files:
 *   ionrad.c
 *   ionrad_3d.c
 *   ionrad_chemistry.c
 *   ionradplane_3d.c
 *   ionradpoint_3d.c
 *============================================================================*/

#include <stdio.h>
#include <stdarg.h>
#include "../athena.h"
#include "../defs.h"

#include "../config.h"

#ifdef MPI_PARALLEL
#include "mpi.h"
#endif

#ifdef ION_RADIATION
/*----------------------------------------------------------------------------*/
/* ionrad.c */
void init_ionrad(MeshS *pM);
void integrate_ionrad_init(MeshS *pM);
void integrate_ionrad(MeshS *pM);
void integrate_ionrad_destruct();

/*----------------------------------------------------------------------------*/
/* ionrad_3d.c */
void ion_radtransfer_3d(DomainS *pD);
void integrate_ionrad_init_3d(MeshS *pM);
void integrate_ionrad_destruct_3d();

/*----------------------------------------------------------------------------*/
/* ionrad_chemistry.c */
Real recomb_rate_coef(Real T);
Real coll_ion_rate_coef(Real T);
Real recomb_cool_rate_coef(Real T);
Real dmc_cool_rate(Real x, Real T);
Real osterbrock_cool_rate(Real T);
Real ki_cool_rate(Real T);
Real ki_heat_rate(void);
Real lya_cool_rate(Real nh, Real nhplus, Real T);
#endif /* ION_RADIATION */

#ifdef ION_RADPLANE
/*----------------------------------------------------------------------------*/
/* ionradplane_3d.c */
void add_radplane_3d(DomainS *pD, int dir, Real flux);
void ion_radplane_init_3d(MeshS *pM);
void get_ph_rate_plane(Radplane *plane, Real ***ph_rate, DomainS *pD);
#endif /* ION_RADPLANE */

#ifdef ION_RADPOINT
/*----------------------------------------------------------------------------*/
/* ionradpoint_3d.c */
void get_ph_rate_point(Real s, Ray_Tree *tree, Real ***ph_rate, GridS *pG);
void add_radpoint_3d(GridS *pG, Real x1, Real x2, Real x3, Real s);
void restore_radpoint_3d(GridS *pG, Real x1, Real x2, Real x3, Real s,
                         int rebuild_ctr, float rotation[3][3]);
void refresh_trees(GridS *pG);
void ion_radpoint_init_3d(DomainS *pM);
Rad_Ran2_State ion_radpoint_get_ranstate(void);
void ion_radpoint_set_ranstate(Rad_Ran2_State newstate);
void ion_radpoint_init_ranstate(void);
#endif /* ION_RADPOINT */

#if defined(STATIC_MESH_REFINEMENT)
/*----------------------------------------------------------------------------*/
/* ionrad_smr.c */
void RestrictCorrect_ionrad(MeshS *pM);
void Prolongate_ionrad_snd(DomainS *pD);
void Prolongate_ionrad_rcv(DomainS *pD);
void SMR_ionrad_init(MeshS *pM);
void SMR_ionrad_destruct();
#endif /* STATIC_MESH_REFINEMENT */

#endif /* IONRAD_PROTOTYPES_H */
