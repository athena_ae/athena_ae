#include "../copyright.h"
/*==============================================================================
 * FILE: ionrad_3d.c
 *
 * PURPOSE: Contains functions to compute an ionization radiative transfer
 *   from update, using the algorithm described in Krumholz, Stone,
 *   & Gardiner (2007).
 *
 *   Use of these routines requires that --enable-ion-radiation be set
 *   at compile time.
 *
 * CONTAINS PUBLIC FUNCTIONS:
 *   ion_radtransfer_3d             - does an ionizing radiative transfer
 *                                      update
 *   integrate_ionrad_init_3d       - handles internal initialization
 *   integrate_ionrad_destruct_3d   - frees all memory taken by ion_init_3d
 *============================================================================*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include "ionrad.h"
#include "prototypes.h"
#include "../prototypes.h"

#ifdef ION_RADIATION

/* Global storage arrays */
static Real ***ph_rate;            /* Photoionization rate */
static Real ***edot;               /* Rate of change of energy */
static Real ***nHdot;              /* Rate of change of neutral
                                      density */
static int ***last_sign;           /* Last sign of nHdot -- keep track
                                      of this to avoid oscillatory
                                      overstability */
static int ***sign_count;          /* Number of successive times the
                                      sign of nHdot has flipped -- use
                                      this to avoid oscillatory
                                      overstability */
static Real ***e_init;             /* Total energies on entry to
                                      routine */
static Real ***e_th_init;          /* Thermal energies on entry to
                                      routine */
static Real ***x_init;             /* Ionization fraction on entry to
                                      routine */

/* ------------------------------------------------------------
 * Photoionization routines
 * ------------------------------------------------------------
 *
 * These routines do the work of computing the photoionization and
 * chemisty update.
 *
 */

/* Routine to zero out initial photoionization rates */
void ph_rate_init(GridS *pG){
  int i, j, k;

  for (k = 0; k < pG->Nx[2]+2*nghost; k++) {
    for (j = 0; j < pG->Nx[1]+2*nghost; j++) {
      for (i = 0; i < pG->Nx[0]+2*nghost; i++) {
        ph_rate[k][j][i] = 0.0;
      }
    }
  }

  return;
}

/* Routine to floor temperatures */
void apply_temp_floor(GridS *pG) {
  int i, j, k;
  Real E_th, KE, T, n_H, n_Hplus, n_e, n_tot;

#ifdef MHD
  Real BE;
#endif

  for (k = 0; k < pG->Nx[2]+2*nghost; k++) {
    for (j = 0; j < pG->Nx[1]+2*nghost; j++) {
      for (i = 0; i < pG->Nx[0]+2*nghost; i++) {
        /* Compute temperature */
        n_H     = pG->U[k][j][i].s[0]/m_H;
        n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_H;
        n_e     = n_Hplus+pG->U[k][j][i].d*alpha_C/(14.0*m_H);
        n_tot   = n_e+n_H+n_Hplus;
        KE = 0.5*
             (pG->U[k][j][i].M1*pG->U[k][j][i].M1+
              pG->U[k][j][i].M2*pG->U[k][j][i].M2+
              pG->U[k][j][i].M3*pG->U[k][j][i].M3)
             /pG->U[k][j][i].d;
#ifdef MHD
        BE = 0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                  pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                  pG->U[k][j][i].B3c*pG->U[k][j][i].B3c);
#endif
        E_th  = pG->U[k][j][i].E-KE;
#ifdef MHD
        E_th -= BE;
#endif
        T = Gamma_1/(k_B*n_tot)*E_th;

        if (T < tfloor || (T > tceil && tceil > 0)) {
          T = T < tfloor ? tfloor : T;
          T = T > tceil ? tceil : T;
          E_th = n_tot*k_B*T/Gamma_1;
          pG->U[k][j][i].E =
            0.5*(pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                 pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                 pG->U[k][j][i].M3*pG->U[k][j][i].M3)
            /pG->U[k][j][i].d
#ifdef MHD
            +0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                  pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                  pG->U[k][j][i].B3c*pG->U[k][j][i].B3c)
#endif
            +E_th;
        }
      }
    }
  }

  return;
}

/* Routine to keep d_n > floor and d_n < d. */
void apply_neutral_floor(GridS *pG) {
  int i, j, k;
  Real d_nlim;

  for (k = 0; k < pG->Nx[2]+2*nghost; k++) {
    for (j = 0; j < pG->Nx[1]+2*nghost; j++) {
      for (i = 0; i < pG->Nx[0]+2*nghost; i++) {
        d_nlim = pG->U[k][j][i].d*IONFRACFLOOR;
        d_nlim = d_nlim < d_nlo ? d_nlim : d_nlo;
        if (pG->U[k][j][i].s[0] < d_nlim) {
          pG->U[k][j][i].s[0] = d_nlim;
        }
        else if (pG->U[k][j][i].s[0] > pG->U[k][j][i].d) {
          pG->U[k][j][i].s[0] = pG->U[k][j][i].d;
        }
      }
    }
  }

  return;
}

/* Routine to save energy, thermal energy, and ionization fraction
   passed to routine -- used for time step constraint. */
void save_energy_and_x(GridS *pG){
  int i, j, k;
  Real E_th, n_H, n_Hplus, x;

  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        /* Compute thermal energy */
        E_th = pG->U[k][j][i].E
#ifdef MHD
               -0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                     pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                     pG->U[k][j][i].B3c*pG->U[k][j][i].B3c)
#endif
               -0.5*((pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                      pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                      pG->U[k][j][i].M3*pG->U[k][j][i].M3)
                     /pG->U[k][j][i].d);
        /* Compute ion fraction */
        n_H = pG->U[k][j][i].s[0]/m_H;
        n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_H;
        x   = n_Hplus/(n_H+n_Hplus);
        x   = x < IONFRACFLOOR ? IONFRACFLOOR : x;
        /* Save thermal and total energy, and neutral fraction */
        e_init[k][j][i] = pG->U[k][j][i].E;
        e_th_init[k][j][i] = E_th;
        x_init[k][j][i] = x;
        /* Initialize last_sign and sign_count arrays */
        last_sign[k][j][i]  = 0;
        sign_count[k][j][i] = 0;
      }
    }
  }

  return;
}

/* Routine to check if we have changed the total energy, thermal
   energy, or x_n as much as we are allowed. */
int check_range(DomainS *pD) {
  GridS *pG = pD->Grid;
  int i, j, k;
  Real E_th, n_H, n_Hplus, x;
  long cellcount = 0;

#ifdef MPI_PARALLEL
  int err;
  long cellcount_glob = 0;
#endif

  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        /* Check D type condition */
        n_H = pG->U[k][j][i].s[0]/m_H;
        if (ph_rate[k][j][i]/(min_area*n_H) > 2.0*CION) {
          continue;
        }
        /* Check thermal energy condition */
        if (max_de_therm_step > 0) {
          E_th = pG->U[k][j][i].E
#ifdef MHD
                 -0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                       pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                       pG->U[k][j][i].B3c*pG->U[k][j][i].B3c)
#endif
                 -0.5*((pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                        pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                        pG->U[k][j][i].M3*pG->U[k][j][i].M3)
                       /pG->U[k][j][i].d);
          if ((E_th/e_th_init[k][j][i] >= 1+max_de_therm_step) ||
              (e_th_init[k][j][i]/E_th >= 1+max_de_therm_step)) {
            cellcount++;
            continue;
          }
        }
        /* Check total energy condition */
        if (max_de_step > 0) {
          if ((pG->U[k][j][i].E/e_init[k][j][i] >= 1+max_de_step) ||
              (e_init[k][j][i]/pG->U[k][j][i].E >= 1+max_de_step)) {
            cellcount++;
            continue;
          }
        }
        /* Check neutral fraction condition */
        if (max_dx_step > 0) {
          n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_H;
          x = n_Hplus/(n_H+n_Hplus);
          x = x < IONFRACFLOOR ? IONFRACFLOOR : x;
          if ((x/x_init[k][j][i] >= 1+max_dx_step) ||
              (x_init[k][j][i]/x >= 1+max_dx_step)) {
            cellcount++;
            continue;
          }
        }
      }
    }
  }

#ifdef MPI_PARALLEL
  err = MPI_Allreduce(&cellcount, &cellcount_glob, 1, MPI_LONG,
                      MPI_SUM, pD->Comm_Domain);
  if (err) {
    ath_error("[check_range]: MPI_Allreduce returned error code %d\n", err);
  }
  cellcount = cellcount_glob;
#endif
  if (cellcount > MAXCELLCOUNT) {
    return 1;
  }
  else{
    return 0;
  }
}

#define MAXSIGNCOUNT 4
#define DAMPFACTOR 0.5
Real compute_chem_rates(DomainS *pD){
  GridS *pG = pD->Grid;
  int i, j, k, n;
  Real n_H, n_Hplus, n_e, n_tot;
  Real E_th, T, d_nlim;
  Real dt_chem, dt_chem1, dt_chem2, dt_chem_min;
  Real ndot_recomb, ndot_ion;

#ifdef MPI_PARALLEL
  int err;
  Real dt_chem_min_glob;
#endif

  /* Initialize chemistry time step to large time step */
  dt_chem_min = LARGE;

  /* Loop over cells to get timestep */
  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        /* Get species abundances */
        n_H     = pG->U[k][j][i].s[0]/m_H;
        n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_H;
        n_e     = n_Hplus+pG->U[k][j][i].d*alpha_C/(14.0*m_H);
//n_e = n_e < pG->U[k][j][i].d*IONFRACFLOOR/m_H ? pG->U[k][j][i].d*IONFRACFLOOR/m_H : n_e;
        n_tot   = n_e+n_H+n_Hplus;
        /* Get gas temperature in K */
        E_th    = pG->U[k][j][i].E
#ifdef MHD
                  -0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                        pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                        pG->U[k][j][i].B3c*pG->U[k][j][i].B3c)
#endif
                  -0.5*((pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                         pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                         pG->U[k][j][i].M3*pG->U[k][j][i].M3)
                        /pG->U[k][j][i].d);
        T = Gamma_1/(n_tot*k_B)*E_th;
        if (T < tfloor) {
          T = tfloor;
        }
        /* Get rate of change of neutral density */
        ndot_recomb = recomb_rate_coef(T)*time_unit*n_e*n_Hplus;
        ndot_ion = ph_rate[k][j][i]*n_H;

        pG->Dots[k][j][i].ndot_HI_recomb += ndot_recomb;
        pG->Dots[k][j][i].ndot_HI_ion -= ndot_ion;

        nHdot[k][j][i] = ndot_recomb-ndot_ion;

//          -coll_ion_rate_coef(T)*time_unit*n_e*n_H
        ;
        /* Check if the sign has flipped -- oscillatory overstability
           check */
        if (nHdot[k][j][i] < 0.0) {
          if (last_sign[k][j][i] == 1) {
            sign_count[k][j][i]++;
          }
          else if (sign_count[k][j][i] > 0) {
            sign_count[k][j][i]--;
          }
          last_sign[k][j][i] = -1;
        }
        else if (nHdot[k][j][i] > 0.0) {
          if (last_sign[k][j][i] == -1) {
            sign_count[k][j][i]++;
          }
          else if (sign_count[k][j][i] > 0) {
            sign_count[k][j][i]--;
          }
          last_sign[k][j][i] = 1;
        }
        else {
          sign_count[k][j][i] = last_sign[k][j][i] = 0;
        }
        /* If sign has flipped too many times successively, this cell
         * is probably experiencing oscillatory overstability. To
         * combat this, decrease the update amount by a chosen factor
         * for every repeat of the same sign past the trigger number
         */
        for (n = MAXSIGNCOUNT; n < sign_count[k][j][i]; n++) {
          edot[k][j][i]  *= DAMPFACTOR;
          nHdot[k][j][i] *= DAMPFACTOR;
          pG->Dots[k][j][i].edot_HI_lya *= DAMPFACTOR;
          pG->Dots[k][j][i].edot_HI_recomb *= DAMPFACTOR;
          pG->Dots[k][j][i].edot_HI_ion *= DAMPFACTOR;
          pG->Dots[k][j][i].edot_pdV *= DAMPFACTOR;
          pG->Dots[k][j][i].edot_advect *= DAMPFACTOR;
          pG->Dots[k][j][i].ndot_HI_recomb *= DAMPFACTOR;
          pG->Dots[k][j][i].ndot_HI_ion *= DAMPFACTOR;
          pG->Dots[k][j][i].ndot_HI_advect *= DAMPFACTOR;
        }
        /* Get ionization fraction floor */
        d_nlim = pG->U[k][j][i].d*IONFRACFLOOR;
        d_nlim = d_nlim < d_nlo ? d_nlim : d_nlo;
        /* Compute chemistry time step for this cell and find the min */
        if (nHdot[k][j][i] == 0.0) {
          dt_chem1 = dt_chem2 = LARGE;
        }
        else if (nHdot[k][j][i] > 0.0) {
          dt_chem1 = max_dx_iter/(1+max_dx_iter)*n_e/nHdot[k][j][i];
          dt_chem2 = max_dx_iter*n_H/nHdot[k][j][i];
        }
        else if (pG->U[k][j][i].s[0] > 1.0001*d_nlim) {
          dt_chem1 = -max_dx_iter*n_e/nHdot[k][j][i];
          dt_chem2 = -max_dx_iter/(1+max_dx_iter)*n_H/nHdot[k][j][i];
        }
        else {
          dt_chem1 = dt_chem2 = LARGE;
        }
        dt_chem = (dt_chem1 < dt_chem2) ? dt_chem1 : dt_chem2;
        dt_chem_min = (dt_chem < dt_chem_min) ? dt_chem : dt_chem_min;
        /* Sanity check */
        if (dt_chem < 0) {
          ath_error(
            "[compute_chem_rates]: proc:%d, lev:%d, dom:%d, cell %d %d %d: dt_chem = %e, d = %e, d_n = %e, T = %e, nH = %e, nH+ = %e, ne = %e, nHdot = %e\n",
            myID_Comm_world, pD->Level, pD->DomNumber, i, j, k, dt_chem,
            pG->U[k][j][i].d, pG->U[k][j][i].s[0], T, n_H, n_Hplus, n_e,
            nHdot[k][j][i]);
        }
      }
    }
  }

#ifdef MPI_PARALLEL
  /* Sync chemistry timestep across processors */
  err = MPI_Allreduce(&dt_chem_min, &dt_chem_min_glob, 1, MP_RL,
                      MPI_MIN, pD->Comm_Domain);
  if (err) {
    ath_error("[compute_chem_rates]: MPI_Allreduce returned error code %d\n"
              , err);
  }
  dt_chem_min = dt_chem_min_glob;
#endif

  /*fclose(fp);*/

  return dt_chem_min;
}
#undef MAXSIGNCOUNT
#undef DAMPFACTOR

Real compute_therm_rates(DomainS *pD){
  GridS *pG = pD->Grid;
  int i, j, k;
  Real n_H, n_Hplus, n_e, n_tot;
  Real T, E_th, E_th_min, E_min, d_nlim;
  Real dt_therm, dt_therm1, dt_therm2, dt_therm_min;
  Real edot_lya, edot_recomb, edot_ion;

#ifdef MPI_PARALLEL
  int err;
  Real dt_therm_min_glob;
#endif

  /* Initialize thermal time step to large value */
  dt_therm_min = LARGE;

  /* Loop over cells to get timestep */
  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        /* Get species abundances */
        n_H     = pG->U[k][j][i].s[0]/m_H;
        n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_H;
        n_e     = n_Hplus+pG->U[k][j][i].d*alpha_C/(14.0*m_H);
        n_tot   = n_e+n_H+n_Hplus;

        /* Get gas temperature in K */
        E_th = pG->U[k][j][i].E
#ifdef MHD
               -0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                     pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                     pG->U[k][j][i].B3c*pG->U[k][j][i].B3c)
#endif
               -0.5*((pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                      pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                      pG->U[k][j][i].M3*pG->U[k][j][i].M3)
                     /pG->U[k][j][i].d);
        T = Gamma_1/(n_tot*k_B)*E_th;
        /* Check temperature floor. If cell is below temperature
           floor, skip it. We'll fix it later */
        if (T < tfloor) {
          edot[k][j][i] = 0.0;
          continue;
        }
        /* If we're at the ionization floor and trying to ionize
           further, we won't update the temperature, so skip this
           cell. */
        d_nlim = pG->U[k][j][i].d*IONFRACFLOOR;
        d_nlim = d_nlim < d_nlo ? d_nlim : d_nlo;
        if ((nHdot[k][j][i] < 0) &&
            (pG->U[k][j][i].s[0] < 1.0001*d_nlim)) {
          edot[k][j][i] = 0.0;
          continue;
        }
        /* Get rate of change of gas energy. Only use molecular cooling
           in cells with < COOLFRAC or > 1 - COOLFRAC ionization fraction, to
           avoid artificial over-cooling in mixed or transition cells. */
        edot_lya = lya_cool_rate(n_H, n_e, T)*time_unit;
        edot_recomb = recomb_cool_rate_coef(T)*time_unit*n_Hplus*n_e;
        edot_ion = ph_rate[k][j][i]*e_gamma*n_H;

        pG->Dots[k][j][i].edot_HI_lya += edot_lya;
        pG->Dots[k][j][i].edot_HI_recomb -= edot_recomb;
        pG->Dots[k][j][i].edot_HI_ion += edot_ion;

        edot[k][j][i] = ph_rate[k][j][i]*e_gamma*n_H
/*                        -osterbrock_cool_rate(T)*n_e*n_Hplus
 */
                        -recomb_cool_rate_coef(T)*time_unit*n_Hplus*n_e
                        +lya_cool_rate(n_H, n_e, T)*time_unit
        ;
/*        if ((n_Hplus/(n_H+n_Hplus) < COOLFRAC) ||
            (n_Hplus/(n_H+n_Hplus) > 1.0-COOLFRAC)) {
          edot[k][j][i] += ki_heat_rate()*time_unit*n_H
                           -ki_cool_rate(T)*time_unit*n_H*n_H;
        }
 */
        /* Compute thermal time step for this cell and find the
           min. Note that, if we're cooling, we need to take into
           account the effect of the floor. */
        if (edot[k][j][i] == 0.0) {
          dt_therm1 = dt_therm2 = LARGE;
        }
        else if (edot[k][j][i] > 0.0) {
          /* We're heating, no need to consider floor */
          dt_therm1 = max_de_iter*pG->U[k][j][i].E/edot[k][j][i];
          dt_therm2 = max_de_therm_iter*E_th/edot[k][j][i];
        }
        else {
          /* We're cooling. Start by computing the total and thermal
             energy the gas would have if it were at the temperature
             floor. */
          E_th_min = n_tot*k_B*tfloor/Gamma_1;
          E_min = E_th_min
#ifdef MHD
                  +0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                        pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                        pG->U[k][j][i].B3c*pG->U[k][j][i].B3c)
#endif
                  +0.5*((pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                         pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                         pG->U[k][j][i].M3*pG->U[k][j][i].M3)
                        /pG->U[k][j][i].d);
          /* If cooling to the temperature floor would not violate the
             constraint on the maximum allowable change in either
             total or thermal energy, there is no constraint, and we
             can continue to the next cell */
          if ((E_th/(1.0+max_de_therm_iter) < E_th_min) &&
              (pG->U[k][j][i].E/(1.0+max_de_iter) < E_min)) {
            continue;
          }
          /* If we're here, cooling to the temperature floor would
             violate our time step constraint. Therefore compute the
             time step normally. */
          dt_therm1 = -max_de_iter/(1+max_de_iter)*pG->U[k][j][i].E
                      /edot[k][j][i];
          dt_therm2 = -max_de_therm_iter/(1+max_de_therm_iter)*E_th/
                      edot[k][j][i];
        }
        /* Set time step to minimum */
        dt_therm = (dt_therm1 < dt_therm2) ? dt_therm1 : dt_therm2;
        dt_therm_min = (dt_therm < dt_therm_min) ? dt_therm : dt_therm_min;
      }
    }
  }

#ifdef MPI_PARALLEL
  /* Sync thermal timestep across processors */
  err = MPI_Allreduce(&dt_therm_min, &dt_therm_min_glob, 1, MP_RL,
                      MPI_MIN, pD->Comm_Domain);
  if (err) {
    ath_error("[compute_therm_rates]: MPI_Allreduce returned error code %d\n"
              , err);
  }
  dt_therm_min = dt_therm_min_glob;
#endif

  return dt_therm_min;
}

void ionization_update(GridS *pG, Real dt){
  int i, j, k;
  Real d_nlim;

  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        d_nlim = pG->U[k][j][i].d*IONFRACFLOOR;
        d_nlim = d_nlim < d_nlo ? d_nlim : d_nlo;
        if ((nHdot[k][j][i] > 0) || (pG->U[k][j][i].s[0] > 1.0001*d_nlim)) {
          /* Update gas energy */
          pG->U[k][j][i].E += edot[k][j][i]*dt;
          /* Update neutral density */
          pG->U[k][j][i].s[0] += nHdot[k][j][i]*dt*m_H;
        }
      }
    }
  }

  return;
}

Real compute_dt_hydro(DomainS *pD) {
  GridS *pG = pD->Grid;
  int i, j, k;
  Real di, v1, v2, v3, qsq, p, asq, cf1sq, cf2sq, cf3sq, max_dti = 0.0, dt;

#ifdef MHD
  Real b1, b2, b3, bsq, tsum, tdif;
#endif /* MHD */
#ifdef MPI_PARALLEL
  Real dt_glob;
  int err;
#endif /* MPI_PARALLEL */

  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        di  = 1.0/(pG->U[k][j][i].d);
        v1  = pG->U[k][j][i].M1*di;
        v2  = pG->U[k][j][i].M2*di;
        v3  = pG->U[k][j][i].M3*di;
        qsq = v1*v1+v2*v2+v3*v3;

#ifdef MHD
        /* Use maximum of face-centered fields (always larger than
           cell-centered B) */
        b1  = pG->U[k][j][i].B1c
              +fabs((double)(pG->B1i[k][j][i]-pG->U[k][j][i].B1c));
        b2  = pG->U[k][j][i].B2c
              +fabs((double)(pG->B2i[k][j][i]-pG->U[k][j][i].B2c));
        b3  = pG->U[k][j][i].B3c
              +fabs((double)(pG->B3i[k][j][i]-pG->U[k][j][i].B3c));
        bsq = b1*b1+b2*b2+b3*b3;
        /* compute sound speed squared */
#ifdef ADIABATIC
        p   = MAX(Gamma_1*(pG->U[k][j][i].E-0.5*pG->U[k][j][i].d*qsq
                           -0.5*bsq), P_MIN);
        asq = Gamma*p*di;
#else
        asq = Iso_csound2;
#endif /* ADIABATIC */
       /* compute fast magnetosonic speed squared in each direction */
        tsum  = bsq*di+asq;
        tdif  = bsq*di-asq;
        cf1sq = 0.5*(tsum+sqrt(tdif*tdif+4.0*asq*(b2*b2+b3*b3)*di));
        cf2sq = 0.5*(tsum+sqrt(tdif*tdif+4.0*asq*(b1*b1+b3*b3)*di));
        cf3sq = 0.5*(tsum+sqrt(tdif*tdif+4.0*asq*(b1*b1+b2*b2)*di));
#else /* MHD */
      /* compute sound speed squared */
#ifdef ADIABATIC
        p   = MAX(Gamma_1*(pG->U[k][j][i].E-0.5*pG->U[k][j][i].d*qsq),
                  P_MIN);
        asq = Gamma*p*di;
#else
        asq = Iso_csound2;
#endif /* ADIABATIC */
       /* compute fast magnetosonic speed squared in each direction */
        cf1sq = asq;
        cf2sq = asq;
        cf3sq = asq;
#endif /* MHD */

        /* compute maximum inverse of dt (corresponding to minimum dt) */
        if (pG->Nx[0] > 1) {
          max_dti = MAX(max_dti, (fabs(v1)+sqrt((double)cf1sq))/pG->dx1);
        }
        if (pG->Nx[1] > 1) {
          max_dti = MAX(max_dti, (fabs(v2)+sqrt((double)cf2sq))/pG->dx2);
        }
        if (pG->Nx[2] > 1) {
          max_dti = MAX(max_dti, (fabs(v3)+sqrt((double)cf3sq))/pG->dx3);
        }
      }
    }
  }

  /* get timestep. */
  dt = CourNo/max_dti;

#ifdef MPI_PARALLEL
  err = MPI_Allreduce(&dt, &dt_glob, 1, MP_RL, MPI_MIN, pD->Comm_Domain);
  if (err) {
    ath_error("[compute_dt_hydro]: MPI_Allreduce returned error code %d\n"
              , err);
  }
  dt = dt_glob;
#endif /* MPI_PARALLEL */
  return dt;
}

/* ------------------------------------------------------------
 * Initialize and store routines
 * ------------------------------------------------------------
 *
 * These are called at problem setup to allocate memory and read
 * setup parameters, or when checkpointing to dump internal data
 * needed on restart.
 *
 */

void integrate_ionrad_init_3d(MeshS *pM) {
  int nl, nd;
  int size1 = 0, size2 = 0, size3 = 0;
  Real area1, area2, area3, maxdx;

  /* Cycle over all Grids on this processor to find maximum Nx1, Nx2, Nx3 */
  for (nl = 0; nl < (pM->NLevels); nl++) {
    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) {
        if (pM->Domain[nl][nd].Grid->Nx[0] > size1) {
          size1 = pM->Domain[nl][nd].Grid->Nx[0];
        }
        if (pM->Domain[nl][nd].Grid->Nx[1] > size2) {
          size2 = pM->Domain[nl][nd].Grid->Nx[1];
        }
        if (pM->Domain[nl][nd].Grid->Nx[2] > size3) {
          size3 = pM->Domain[nl][nd].Grid->Nx[2];
        }
      }
    }
  }

  size1 = size1+2*nghost;
  size2 = size2+2*nghost;
  size3 = size3+2*nghost;

  /* Read input values  */
  sigma_ph = par_getd("ionradiation", "sigma_ph");
  m_H = par_getd("ionradiation", "m_H");
  mu  = par_getd("ionradiation", "mu");
  e_gamma     = par_getd("ionradiation", "e_gamma");
  alpha_C     = par_getd("ionradiation", "alpha_C");
  k_B = par_getd("ionradiation", "k_B");
  time_unit   = par_getd("ionradiation", "time_unit");
  max_de_iter = par_getd("ionradiation", "max_de_iter");
  max_de_therm_iter = par_getd("ionradiation", "max_de_therm_iter");
  max_dx_iter = par_getd("ionradiation", "max_dx_iter");
  max_de_step = par_getd("ionradiation", "max_de_step");
  max_de_therm_step = par_getd("ionradiation", "max_de_therm_step");
  max_dx_step = par_getd("ionradiation", "max_dx_step");
  tfloor  = par_getd("ionradiation", "tfloor");
  tceil   = par_getd("ionradiation", "tceil");
  maxiter = par_geti("ionradiation", "maxiter");

  /* Allocate memory for rate arrays */
  ph_rate = (Real***)
            calloc_3d_array(size3, size2, size1, sizeof(Real));
  edot  = (Real***)
          calloc_3d_array(size3, size2, size1, sizeof(Real));
  nHdot = (Real***)
          calloc_3d_array(size3, size2, size1, sizeof(Real));
  last_sign  = (int***)
               calloc_3d_array(size3, size2, size1, sizeof(int));
  sign_count = (int***)
               calloc_3d_array(size3, size2, size1, sizeof(int));
  e_init = (Real***)
           calloc_3d_array(size3, size2, size1, sizeof(Real));
  e_th_init = (Real***)
              calloc_3d_array(size3, size2, size1, sizeof(Real));
  x_init = (Real***)
           calloc_3d_array(size3, size2, size1, sizeof(Real));

#ifdef ION_RADPOINT
  /* Initialize random number generator if this is not a restart. If
     this is a restart, we have already set up the random number
     generator from the restart file. */
  if (pM.nstep != 0) {
    ion_radpoint_init_ranstate();
  }
#endif /* ION_RADPOINT */

  /* What's the smallest area a cell face can have? */
  area1 = pM->dx[0]*pM->dx[1];
  area2 = pM->dx[0]*pM->dx[2];
  area3 = pM->dx[1]*pM->dx[2];
  if (area1 < area2) {
    if (area1 < area3) {
      min_area = area1;
    }
    else{
      min_area = area3;
    }
  }
  else {
    if (area2 < area3) {
      min_area = area2;
    }
    else{
      min_area = area3;
    }
  }
  min_area /= SQR(pow(2., pM->NLevels));

  /* What's the "low" neutral density, corresponding to the minimum
     optical depth we care about? */
  maxdx  = pM->dx[0] > pM->dx[1] ? pM->dx[0] : pM->dx[1];
  maxdx  = maxdx > pM->dx[2] ? maxdx : pM->dx[2];
  maxdx /= pow(2., pM->NLevels);
  d_nlo  = MINOPTDEPTH*m_H/(sigma_ph*maxdx);

  return;
}

void integrate_ionrad_destruct_3d(){
  if (ph_rate != NULL) {
    free_3d_array(ph_rate);
  }
  if (edot != NULL) {
    free_3d_array(edot);
  }
  if (nHdot != NULL) {
    free_3d_array(nHdot);
  }
  if (last_sign != NULL) {
    free_3d_array(last_sign);
  }
  if (sign_count != NULL) {
    free_3d_array(sign_count);
  }
  if (e_init != NULL) {
    free_3d_array(e_init);
  }
  if (e_th_init != NULL) {
    free_3d_array(e_th_init);
  }
  if (x_init != NULL) {
    free_3d_array(x_init);
  }

  return;
}

/* ------------------------------------------------------------
 * Main integration routine
 * ------------------------------------------------------------
 *
 * This is the driver routine for the radiation integration step.
 *
 */

void ion_radtransfer_3d(DomainS *pD) {
  GridS *pG = pD->Grid;
  Real dt_chem, dt_therm, dt_hydro, dt, dt_done;
  int i, j, k;
  unsigned int n;
  int niter, hydro_done;
  int nchem, ntherm;

#ifdef STATIC_MESH_REFINEMENT
  /*If not on root domain, wait to receive flux. */
  if (pD->Level != 0) {
    Prolongate_ionrad_rcv(pD);
  }
#endif

#ifdef ION_RADPOINT
  /* Build or rebuild trees */
  refresh_trees(pG);
#endif /* ION_RADPOINT */

  /* Set neutral densities below floor to sensible values. This is necessary
     because the hydro update can make the neutral density negative. */
  apply_neutral_floor(pG);

  /* Set all temperatures below the floor to the floor */
  apply_temp_floor(pG);

  /* Save total and thermal energies and neutral fractions passed in
     -- we use these to compute time steps. Also initialize the last_sign
     and sign_count arrays */
  save_energy_and_x(pG);

#if defined(MPI_PARALLEL) && defined(ION_RADPOINT)
  /* Allocate buffer for MPI communication and attach here. Note that
     we detatch below, to ensure that the buffer we use here doesn't
     get tangled up with buffers that may be used in other
     modules. This is necessary because only one MPI buffer is allowed
     per process. We only use the buffer for point sources, since it
     is easier to handle plane fronts using non-buffered communication. */
  if (mpi_bufsize == 0) {
    mpi_bufsize = INIT_MPI_BUFSIZE;
    mpi_buffer  = malloc(mpi_bufsize);
  }
  MPI_Buffer_attach(mpi_buffer, mpi_bufsize);
#endif

  /* Begin the radiation sub-cycle */
  dt_done = 0.0;
  hydro_done = 0;
  nchem   = 0;
  ntherm  = 0;
  niter   = 0;

  for (k = pG->ks; k <= pG->ke; k++) {
    for (j = pG->js; j <= pG->je; j++) {
      for (i = pG->is; i <= pG->ie; i++) {
        pG->Dots[k][j][i].edot_HI_lya = 0.0;
        pG->Dots[k][j][i].edot_HI_recomb = 0.0;
        pG->Dots[k][j][i].edot_HI_ion = 0.0;
        pG->Dots[k][j][i].edot_advect = 0.0;
        pG->Dots[k][j][i].edot_pdV = 0.0;
        pG->Dots[k][j][i].ndot_HI_recomb = 0.0;
        pG->Dots[k][j][i].ndot_HI_ion = 0.0;
        pG->Dots[k][j][i].ndot_HI_advect = 0.0;
      }
    }
  }

  while (dt_done < pG->dt) {
    /* Have we exceeded the maximum number of iterations? If so, return
       to hydro with the time step re-set to what we managed to do.
       Only restrict root to maxiter, infinte loop should be avoided as
       long as dt is never 0. */
    if (pD->Level == 0 && niter == maxiter) {
      pG->dt = dt_done;
      break;
    }

    /* Initialize photoionization rate array */
    ph_rate_init(pG);

    /* Compute photoionization rate from all sources */
#ifdef ION_RADPOINT
    for (n = 0; n < pG->nradpoint; n++) {
      get_ph_rate_point(pG->radpointlist[n].s,
                        &(pG->radpointlist[n].tree),
                        ph_rate, pD);
    }
#endif
#ifdef ION_RADPLANE
    for (n = 0; n < pG->nradplane; n++) {
      get_ph_rate_plane((Radplane*)&(pG->radplanelist[n]), ph_rate, pD);
    }
#endif

    /* Compute rates and time step for chemistry update */
    dt_chem  = compute_chem_rates(pD);
    /* Compute rates and time step for thermal energy update */
    dt_therm = compute_therm_rates(pD);
    /* Set time step to smaller of thermal and chemical time
       steps, and record whether this is a thermal or chemical step */
    if (dt_chem < dt_therm) {
      nchem++;
    }
    else{
      ntherm++;
    }
    dt = MIN(dt_therm, dt_chem);
    if (dt <= 0) {
      ath_error(
        "Require strictly positive dt. On level %d got dt=%e from dt_therm=%e and dt_chem=%e.\n", pD->Level, dt, dt_therm,
        dt_chem);
    }
    /* If necessary, scale back time step to avoid exceeding hydro
       time step. */
    if (dt_done+dt > pG->dt) {
      dt = pG->dt-dt_done;
      hydro_done = 1;
    }

    /* Do an update */
    ionization_update(pG, dt);
    dt_done += dt;
    niter++;
    /* Set all temperatures below the floor to the floor */
    apply_neutral_floor(pG);
    apply_temp_floor(pG);

    /* Check new energies and ionization fractions against initial
       values to see if we've changed them as much as possible. If so,
       exit loop. Only if on root domain. */
    if (pD->Level == 0 && check_range(pD)) {
      pG->dt = dt_done;
      break;
    }

    /* Have we advanced the full hydro time step? If so, exit loop. */
    if (hydro_done) {
      break;
    }

    /* Compute a new hydro time step based on the new temperature
       distribution. If it's smaller than the time step we've already
       advanced, then exit. Only if on root domain. */
    if (pD->Level == 0) {
      dt_hydro = compute_dt_hydro(pD);
      if (dt_hydro < dt_done) {
        pG->dt = dt_done;
        break;
      }
    }
    /* Else do another subcycle */
  }

#if defined(MPI_PARALLEL) && defined(ION_RADPOINT)
  /* Deallocate MPI buffer */
  MPI_Buffer_detach(&mpi_buffer, &mpi_bufsize);
#endif

#ifdef STATIC_MESH_REFINEMENT
  /*Send radiation flux to finer grids that overlap*/
  if (pG->NCGrid != 0) {
    Prolongate_ionrad_snd(pD);
  }
#endif

  /* One grid from each level write status */
  if (pD->GData[0][0][0].ID_Comm_world == myID_Comm_world) {
    ath_pout(-1,
             "Radiation on lev:%d done in %03d iterations: %03d thermal, %03d chemical; new dt = %e\n",
             pD->Level, niter, ntherm, nchem, pG->dt);
  }

  /* Sanity check */
  if (pG->dt < 0) {
    ath_error(
      "[ion_radtransfer_3d]: dt = %e, dt_chem = %e, dt_therm = %e, dt_hydro = %e, dt_done = %e\n",
      pG->dt, dt_chem, dt_therm, dt_hydro, dt_done);
  }
}

#endif /* ION_RADIATION */
