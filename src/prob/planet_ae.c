/*=============================================================================
 *! \file planet_ae.c                                                         *
 *  \brief A hydrostatic spherical adiabatic atmosphere with incoming plane   *
 *         paralell ionizing flux and stellar wind in a rotating frame.       *
 *         USE MACROS TO CONTROL DESIRED FEATURES.                            *
 *                                                                            *
 * PURPOSE: Initalize a grid in which we can launch a planetary wind via      *
 *          stellar irradiation to create a hydrodynamicallu escaping         *
 *          atmosphere. Generates inner boundary coniditions for the planet   *
 *          to continuously supply the wind base with material from which to  *
 *          draw upon. Sets up stellar wind boundary conidtions, and dipole   *
 *          if in the rotating frame.                                         *
 *                                                                            *
 * AUTHORS: John McCann                                                       *
 *                                                                            *
 * Variables: XXX0 - variable at planet's surface (our chosen reference point)*
 *            XXXmask - variable relating the masking radius                  *
 *            XXXib/ob - inner/outter (base/edge) boundary variable           *
 *            XXX1 - first ambient variable                                   *
 *            XXX2 - second ambient variable                                  *
 *            scalar[0] - neutral hydrogen                                    *
 *            scalar[1] - planetary gas                                       *
 *============================================================================*/

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

/* Macros: Add/Remove "NO_" prefix to turn off/on, e.g., NO_PLANET */
#define PLANET /* Places a planet at the center */
#define TIDAL_FORCES /* Controls stellar gravity and centrifugal term */
#define RESTRICT /* If want to use dipole BC or not */
#define STELLAR_WIND /* Sets up a stellar wind w/appropriate BC */

#if !defined(STELLAR_WIND) && !defined(PLANET)
#error "Must have either a planet or stellar wind"
#endif

#if defined(SHEARING_BOX) || defined(TIDAL_FORCES) || defined(CORILOS)
#define ROTATING
#endif

/* static variables used throughout this file */
static Real Ggrav, k_B, m_HI, m_HII;
static PrimS* stellar_wind_0; /* Used for ambient scalars too */

#if defined(PLANET)
static Real GM, r0, nrho, nprs;
static Real Emask, rhomask;
static Real E0, rho0, phi0;
static Real rib, rmask;
#endif

#if defined(ROTATING)
static Real GM_star, adist, Rc, Omega, S_rmask;
#elif defined(STELLAR_WIND)
static Real GM_star, adist, S_rmask;
#endif

#if defined(RESTRICT)
static Real E2, rho2;
#endif

#if defined(STELLAR_WIND)
static Real S_rib;
static Real r_star_wind, b_star_wind, cs02_star_wind;
static Real upbnd, lobnd;
/* Boundary conditions arrays */
static PrimS ***stellar_wind_ix1, ***stellar_wind_ox1;
static PrimS ***stellar_wind_ix2, ***stellar_wind_ox2;
static PrimS ***stellar_wind_ix3, ***stellar_wind_ox3;
static ConsS ***cons_stellar_wind_ix1, ***cons_stellar_wind_ox1;
static ConsS ***cons_stellar_wind_ix2, ***cons_stellar_wind_ox2;
static ConsS ***cons_stellar_wind_ix3, ***cons_stellar_wind_ox3;
#endif

/* Struct for solving for roots of equation via Newton-Raphson */
typedef struct Newton_S {
  Real (*f)(const Real x);
  Real (*df)(const Real x);
} NewtonS;

/*----------------------------------------------------------------------------*
 *======================== PRIVATE FUNCTION PROTOTYPES =======================*
 *----------------------------------------------------------------------------*/

/*======================== GENERAL PURPOSE FUNCTIONS =========================*/
static Real incomplete_beta(Real x, Real a, Real b);
static Real regularized_incomplete_beta(Real x, Real a, Real b);
static Real bisectnewt(NewtonS fS, Real xlo, Real xhi, const Real eps);
/*========================== POTENTIAL FUNCTIONS =============================*/
static Real mech_potenial(const Real x1, const Real x2, const Real x3);
static Real UnstratifiedDisk(const Real x1, const Real x2, const Real x3);
/*============================ OUTPUT FUNCTIONS ==============================*/
#if defined(ION_RADPLANE)
static Real ionfrac(const GridS *pG, const int i, const int j, const int k);
#if !defined(WRITE_GHOST_CELLS)
static Real gas_temp(const GridS *pG, const int i, const int j, const int k);
static Real plane_tau(const GridS *pG, const int i, const int j, const int k);
#endif
static Real ion_flux(const GridS *pG, const int i, const int j, const int k);
static Real edot_HI_lya(const GridS *pG, const int i, const int j, const int k);
static Real edot_HI_recomb(const GridS *pG, const int i, const int j,
                           const int k);
static Real edot_HI_ion(const GridS *pG, const int i, const int j, const int k);
static Real edot_pdV(const GridS *pG, const int i, const int j, const int k);
static Real edot_advect(const GridS *pG, const int i, const int j, const int k);
static Real ndot_HI_recomb(const GridS *pG, const int i, const int j,
                           const int k);
static Real ndot_HI_ion(const GridS *pG, const int i, const int j, const int k);
static Real ndot_HI_advect(const GridS *pG, const int i, const int j,
                           const int k);
static Real cell_dt(const GridS *pG, const int i, const int j, const int k);
#endif
#if defined(STELLAR_WIND)
/*========================= STELLAR WIND FUNCTIONS ===========================*/
static void init_stellar_wind(DomainS *pD);
static Real bernFunc(const Real vel);
static Real dbernFunc(const Real vel);
static Real maxFunc(const Real vel);
static Real dmaxFunc(const Real vel);
/*========================== BOUNDARY FUNCTIONS ==============================*/
static void stellar_wind_bc_ix1(GridS *pG);
static void stellar_wind_bc_ix2(GridS *pG);
static void stellar_wind_bc_ix3(GridS *pG);
static void stellar_wind_bc_ox1(GridS *pG);
static void stellar_wind_bc_ox2(GridS *pG);
static void stellar_wind_bc_ox3(GridS *pG);
#endif
#if defined(RESTRICT)
static void restrict_inflow_bc_ox1(GridS *pG);
static void restrict_inflow_bc_ox2(GridS *pG);
static void restrict_inflow_bc_ox3(GridS *pG);
static void restrict_inflow_bc_ix1(GridS *pG);
static void restrict_inflow_bc_ix2(GridS *pG);
static void restrict_inflow_bc_ix3(GridS *pG);
#endif

/*=============================================================================
 *=========================== PUBLIC FUNCTIONS ================================
 *=============================================================================
 * problem               - set initial conditions for problem                 *
 * problem_write_restart - writes problem-specific user data to restart files *
 * problem_read_restart  - reads problem-specific user data from restart files*
 * get_usr_expr          - sets pointer to expression for special output data *
 * get_usr_out_fun       - returns a user defined output function pointer     *
 * get_usr_par_prop      - returns a user defined particle selection function *
 * Userwork_in_loop      - problem specific work IN     main loop             *
 * Userwork_after_loop   - problem specific work AFTER  main loop             *
 *----------------------------------------------------------------------------*/

/*=============================================================================
 *! \fn void problem(DomainS *pD)                                        *
 *  \brief Setup planet and ambient (either stellar wind or low density gas). *
 *         Reads in <problem> block athinput parameters to ATHENA.            *
 *----------------------------------------------------------------------------*/

void problem(DomainS *pD){
  /* declarations */
  GridS *pG = pD->Grid;
  int i, is = pG->is, ie = pG->ie;
  int j, js = pG->js, je = pG->je;
  int k, ks = pG->ks, ke = pG->ke;
  Real rad, x1, x2, x3;

  Real Ms;
  Real n_star_wind, vel_star_wind, T_star_wind;

#if defined(PLANET)
  Real Mp, T0;
  Real Rcrit, Rz, Rz1, beta, Gscale, n0, sigma_pHI;
  Real phimask, phi1, phi2, phi;
  Real E1, rho1;
  Real fmask, f1, f2, frad;
  Real r1, r2, rob, rlow, refine, dxMIN, dxMAX, NLevels;
#endif

#if !defined(RESTRICT) /* else static global */
  Real E2, rho2;
#endif

#if defined(STELLAR_WIND)
  PrimS stellar_wind;
  NewtonS fS;
  fS.f  = bernFunc;
  fS.df = dbernFunc;
  Real stellar_wind_vel;
  Real c_ph, s_ph, c_th, s_th;
#endif

#if defined(ROTATING)
  Real rhill;
#endif

#if (NSCALARS > 0)
  int s;
#endif

#if defined(ION_RADIATION)
  int radplanecount, nradplanes;
  Real flux;
#endif

  /* Constants */
  Ggrav = par_getd_def("constants", "Ggrav", 6.67259e-8);
  k_B   = par_getd_def("constants", "k_B", 1.380658e-16);
  m_HI  = par_getd_def("constants", "m_HI", 1.673528e-24);
  m_HII = par_getd_def("constants", "m_HII", 1.672621898e-24);

#if defined(PLANET)
  /* User set atmopshere reference variables. Default: Tripathi 2015 */
  r0 = par_getd_def("problem", "r0", 1.5e10);
  Mp = par_getd_def("problem", "Mp", 1.0e30);
  T0 = par_getd_def("problem", "T0", 1.1e3);

  /* Atmosphere boundaries */
  rmask   = par_getd("problem", "rmask");
  rib     = par_getd("problem", "rib");
  rob     = par_getd("problem", "rob");
  /* Assuming square cells */
  dxMAX   = pD->dx[0]/r0*pow(2., pD->Level);
  NLevels = par_geti("job", "num_domains");
  dxMIN   = dxMAX/pow(2., NLevels-1.);
  rlow    = (ceil((2.*((floor(rob*100.)/100.)-1.)+1.)/dxMAX)+NLevels-2)*dxMAX;

  /* Derived planet quantities */
  GM   = Ggrav*Mp;
  nrho = 1.0/Gamma_1;
  nprs = Gamma/Gamma_1;
#endif

#if defined(STELLAR_WIND) || defined(ROTATING)
  /* User set stellar reference variables. Default: Solar at .05 AU */
  adist   = par_getd_def("problem", "adist", 7.48e11);
  Ms = par_getd_def("problem", "Ms", 1.99e33);
  GM_star = Ggrav*Ms;
  S_rmask = par_getd("problem", "S_rmask");
#if defined(STELLAR_WIND)
  S_rib   = par_getd("problem", "S_rib");
#endif
#if defined(ROTATING)
  /* Derived stellar quantities */
  Omega   = sqrt((GM_star+GM)/pow(adist, 3.));
  Rc = adist*Ms/(Ms+Mp);
  rhill   = adist*pow(Mp/(3.*Ms), 1./3.);
#if defined(SHEARING_BOX)
  Omega_0 = Omega;
  qshear  = 0.0;
  ShBoxCoord = xy;
  ShearingBoxPot = UnstratifiedDisk;
#endif
#endif
#endif

#if defined(PLANET)
  /* Set number density at rp such that tau=1 there */
  Rcrit  = Gamma_1*Ggrav*Mp*m_HI/(Gamma*k_B*T0*r0);
  Rz = Rcrit/(Rcrit-1.);
  beta   = tgamma(1.-nrho)*tgamma(1.+nrho)/tgamma(2.);
  Gscale = Rz*pow(Rz-1., -nrho)*beta
           *(1.-regularized_incomplete_beta(1./Rz, 1.-nrho, 1+nrho));

  sigma_pHI = par_getd_def("ionradiation", "sigma_pHI", 6.3e-18);
  n0   = 1./(Gscale*sigma_pHI*r0);
  rho0 = n0*m_HI;
  E0   = n0*k_B*T0/Gamma_1;
  phi0 = mech_potenial(-r0, 0.0, 0.0);
  phimask = mech_potenial(-(rmask+dxMIN/2.)*r0, 0.0, 0.0);

  /* Density and pressure at masking boundary */
  fmask   = 1.+rho0*(phi0-phimask)/(Gamma*E0);
  rhomask = rho0*pow(fmask, nrho);
  Emask   = E0*pow(fmask, nprs);
#endif

  /* Stellar wind parameters */
  n_star_wind   = par_getd_def("problem", "ns", 2.4e3);
  vel_star_wind = par_getd_def("problem", "vs", 2.5e7);
  T_star_wind   = par_getd_def("problem", "Ts", 1.5e6);
#ifdef MHD
  Real B_star_wind = par_getd_def("problem", "Bs", 1.0e-2);
#endif

  /* Derived stellar wind quantities */
  if ((stellar_wind_0 = (PrimS*)calloc_1d_array(1, sizeof(PrimS))) == NULL) {
    ath_error("[hse_planet]: Failed to allocate stellar wind array\n");
  }
  stellar_wind_0->d  = n_star_wind*m_HII;
  stellar_wind_0->V1 = vel_star_wind;
  stellar_wind_0->V2 = 0.0;
  stellar_wind_0->V3 = 0.0;
#ifndef BAROTROPIC
  stellar_wind_0->P  = 2*n_star_wind*k_B*T_star_wind;
#endif
#ifdef MHD
  stellar_wind_0->B1c = B_star_wind;
  stellar_wind_0->B2c = 0.0;
  stellar_wind_0->B3c = 0.0;
#endif
#if (NSCALARS > 0)
  for (s = 0; s < NSCALARS; s++) {
    switch (s) {
    case 0:   /* neutral H */
      stellar_wind_0->r[s] = 1.0e-10;
      break;
      ;
      ;
    case 1:   /* planetary gas */
      stellar_wind_0->r[s] = 0.0;
      break;
      ;
      ;
    default:
      stellar_wind_0->r[s] = -1.0;
      ;
      ;
    }
  }
#endif

  r1   = par_getd("problem", "r1")*r0;
  rho1 = par_getd("problem", "rho1");
  E1   = par_getd("problem", "E1");
#if !defined(STELLAR_WIND)
  r2   = par_getd("problem", "r2")*r0;
  rho2 = par_getd("problem", "rho2");
  E2   = par_getd("problem", "E2");
  phi1 = mech_potenial(-r1, 0, 0);
  phi2 = mech_potenial(-r2, 0, 0);
#else /* defined(STELLAR_WIND) */
  /* Bernoulli constant of stellar wind */
  b_star_wind = 0.5*(SQR(stellar_wind_0->V1)+SQR(stellar_wind_0->V2)
                     +SQR(stellar_wind_0->V3));
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  b_star_wind -= GM_star/S_rmask;
#endif
#ifndef BAROTROPIC
  cs02_star_wind  = Gamma*stellar_wind_0->P/stellar_wind_0->d;
#else /* ISOTHERMAL */
  cs02_star_wind += 2.*k_B*T_star_wind/m_HII;
#endif
  b_star_wind += cs02_star_wind/Gamma_1;

  /* Sets BCs and solves for stellar wind array */
  init_stellar_wind(pD);
#endif

/*=========================== GRID INITIALIZATION =============================*/
  for (k = ks; k <= ke+1; k++) {
    for (j = js; j <= je+1; j++) {
      for (i = is; i <= ie+1; i++) {
        cc_pos(pG, i, j, k, &x1, &x2, &x3);
        rad = sqrt(x1*x1+x2*x2+x3*x3);
#if defined(PLANET)
        phi = mech_potenial(x1, x2, x3);
#endif

/*============================= AMBIENT SETUP ================================*/
/* Stellar wind, built on the assumption of stellar potential exisiting */
#if defined(STELLAR_WIND)
        r_star_wind = MAX(S_rmask, sqrt(SQR(x1+adist)+x2*x2+x3*x3));
        c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
        s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
        c_th = x3/r_star_wind;
        s_th = sqrt(1-SQR(x3/r_star_wind));

        stellar_wind_vel = bisectnewt(fS,
                                      lobnd/
                                      pow(r_star_wind,
                                          2.*Gamma_1/(Gamma+1.)), upbnd, 1.0);
        PrimS stellar_wind;
        stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
        stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
        stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
        stellar_wind.V1 += x2*Omega_0;
        stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
        stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/stellar_wind_vel
                           *SQR(S_rmask/r_star_wind);
        stellar_wind.P   = stellar_wind_0->P
                           *pow(stellar_wind.d/stellar_wind_0->d, Gamma);

        pG->U[k][j][i] = Prim_to_Cons(&stellar_wind);
#else /* !defined(STELLAR_WIND) && defined(PLANET) */
        if (rad < r1) {
        }
        else if (rad < r2) {
          frad = 1.+rho1*(phi1-phi)/(Gamma*E1);
          pG->U[k][j][i].M1 = 0.0;
          pG->U[k][j][i].M2 = 0.0;
          pG->U[k][j][i].M3 = 0.0;
          if (frad > 1e-3) {
            pG->U[k][j][i].d = rho1*pow(frad, nrho);
            pG->U[k][j][i].E = E1*pow(frad, nprs);
          }
          else {
            pG->U[k][j][i].d = rho2;
            pG->U[k][j][i].E = E2;
          }
        }
        else{
          frad = 1.+rho2*(phi2-phi)/(Gamma*E2);
          pG->U[k][j][i].d  = rho2*pow(frad, nrho);
          pG->U[k][j][i].M1 = 0.0;
          pG->U[k][j][i].M2 = 0.0;
          pG->U[k][j][i].M3 = 0.0;
          pG->U[k][j][i].E  = E2*pow(frad, nprs);
        }
#endif /* STELLAR_WIND */
/* Ambient scalars, same for all setups. Ionized and not planetary gas. */
#if (NSCALARS > 0)
        for (s = 0; s < NSCALARS; s++) {
          pG->U[k][j][i].s[s] = pG->U[k][j][i].d*stellar_wind_0->r[s];
        }
#endif
/*============================== PLANET SETUP ================================*/
/* add in planet if ambient flow out to prevent collapse */
#if defined(PLANET)
        if (rad <= rmask*r0) {
          pG->U[k][j][i].d  = rhomask;
          pG->U[k][j][i].M1 = 0.0;
          pG->U[k][j][i].M2 = 0.0;
          pG->U[k][j][i].M3 = 0.0;
          pG->U[k][j][i].E  = Emask;
/* Planet scalars, same for all setups. Neutral and planetary gas. */
#if (NSCALARS > 0)
          for (s = 0; s < NSCALARS; s++) {
            pG->U[k][j][i].s[s] = pG->U[k][j][i].d;
          }
#endif
        }
        else if (rad < r1) {
          pG->U[k][j][i].M1 = 0.0;
          pG->U[k][j][i].M2 = 0.0;
          pG->U[k][j][i].M3 = 0.0;
          frad = 1.+rho0*(phi0-phi)/(Gamma*E0);
          if (frad > 1.e-3) {
            pG->U[k][j][i].d = rho0*pow(frad, nrho);
            pG->U[k][j][i].E = E0*pow(frad, nprs);
/* Planet scalars, same for all setups. Neutral and planetary gas. */
#if (NSCALARS > 0)
            for (s = 0; s < NSCALARS; s++) {
              pG->U[k][j][i].s[s] = pG->U[k][j][i].d;
            }
#endif
          }
          else {
            pG->U[k][j][i].d = rho1;
            pG->U[k][j][i].E = E1;
          }
        }
#endif /* PLANET */
/*============================== ERROR CHECK =================================*/
/* Check that you do not intialize with errors */
        if ((pG->U[k][j][i].d <= 0) || isnan(pG->U[k][j][i].d)) {
          ath_error("Initalized with Neg or NaN d: %e, frad: %e, phi: %e at "  \
                    "|(%.02e, %.02e, %.02e)|=%.02e. proc:%d, i:%d j:%d k:%d, " \
                    "lev:%d \n", pG->U[k][j][i].d, frad, phi, x1, x2, x3, rad,
                    myID_Comm_world, i, j, k, pD->Level);
        }
        if (isnan(pG->U[k][j][i].M1)) {
          ath_error("Initalized with NaN M1: %e at "                           \
                    "|(%.02e, %.02e, %.02e)|=%.02e. proc:%d, i:%d j:%d k:%d, " \
                    "lev:%d \n", pG->U[k][j][i].M1, x1, x2, x3, rad,
                    myID_Comm_world, i, j, k, pD->Level);
        }
        if (isnan(pG->U[k][j][i].M2)) {
          ath_error("Initalized with NaN M2: %e at "                           \
                    "|(%.02e, %.02e, %.02e)|=%.02e. proc:%d, i:%d j:%d k:%d, " \
                    "lev:%d \n", pG->U[k][j][i].M2, x1, x2, x3, rad,
                    myID_Comm_world, i, j, k, pD->Level);
        }
        if (isnan(pG->U[k][j][i].M3)) {
          ath_error("Initalized with NaN M3: %e at "                           \
                    "|(%.02e, %.02e, %.02e)|=%.02e. proc:%d, i:%d j:%d k:%d, " \
                    "lev:%d \n", pG->U[k][j][i].M3, x1, x2, x3, rad,
                    myID_Comm_world, i, j, k, pD->Level);
        }
        if ((pG->U[k][j][i].E <= 0) || isnan(pG->U[k][j][i].E)) {
          ath_error("Initalized with Neg or NaN E: %e at "                     \
                    "|(%.02e, %.02e, %.02e)|=%.02e. proc:%d, i:%d j:%d k:%d, " \
                    "lev:%d \n", pG->U[k][j][i].E, x1, x2, x3, rad,
                    myID_Comm_world, i, j, k, pD->Level);
        }
#if (NSCALARS > 0)
        for (s = 0; s < NSCALARS; s++) {
          if ((pG->U[k][j][i].s[s] < 0) || isnan(pG->U[k][j][i].s[s])) {
            ath_error("Initalized with Neg or NaN s[%d]: %e at "                \
                      "|(%.02e, %.02e, %.02e)|=%.02e. proc:%d, i:%d j:%d k:%d," \
                      " lev:%d\n", s, pG->U[k][j][i].s[s], x1, x2, x3, rad,
                      myID_Comm_world, i, j, k, pD->Level);
          }
        }
#endif
      }
    }
  }

  StaticGravPot = mech_potenial;

#if !defined(STELLAR_WIND) && defined(RESTRICT)
  if (pD->Disp[0] == 0) {
    bvals_mhd_fun(pD, left_x1, restrict_inflow_bc_ix1);
  }
  if (pD->MaxX[0] == pD->RootMaxX[0]) {
    bvals_mhd_fun(pD, right_x1, restrict_inflow_bc_ox1);
  }
  if (pD->Disp[1] == 0) {
    bvals_mhd_fun(pD, left_x2, restrict_inflow_bc_ix2);
  }
  if (pD->MaxX[1] == pD->RootMaxX[1]) {
    bvals_mhd_fun(pD, right_x2, restrict_inflow_bc_ox2);
  }
  if (pD->Disp[2] == 0) {
    bvals_mhd_fun(pD, left_x3, restrict_inflow_bc_ix3);
  }
  if (pD->MaxX[2] == pD->RootMaxX[2]) {
    bvals_mhd_fun(pD, right_x3, restrict_inflow_bc_ox3);
  }
#endif

#ifdef ION_RADIATION
  flux = par_getd("ionradiation", "flux");
  radplanecount = 0;
  nradplanes = par_geti("ionradiation", "nradplanes");
  if (nradplanes != 1) {
    ath_error(0, "This code only considers one 1 radplane, please reconsider.");
  }

  for (i = 0; i < nradplanes; i++) {
    add_radplane_3d(pD, -1, flux);
    radplanecount++;
  }
#endif

  return;
}

/*=============================================================================
 *! \fn void problem_write_restart(MeshS *pM, FILE *fp)                       *
 *  \brief                                                                    *
 *----------------------------------------------------------------------------*/

void problem_write_restart(MeshS *pM, FILE *fp){
  return;
}

/*=============================================================================
 *! \fn void problem_read_restart(MeshS *pM, FILE *fp)                        *
 *  \brief Read in <problem> block variables from athinput file and calculate *
 *         problem variables again and set boundary conditions                *
 *----------------------------------------------------------------------------*/

void problem_read_restart(MeshS *pM, FILE *fp) {
  Real Ms;
  Real n_star_wind, vel_star_wind, T_star_wind;

#if defined(PLANET)
  Real Mp, T0;
  Real Rcrit, Rz, beta, Gscale, n0, sigma_pHI;
  Real phimask;
  Real fmask;
#endif

#if defined(RESTRICT) && !defined(STELLAR_WIND)
  Real rob, rlow, Rz1, refine, dxMIN, dxMAX;
  Real phi1, phi2, phi1z, phi2z;
  Real f1, f2, E1, rho1;
#endif
#if defined(ROTATING)
  Real rhill;
#endif

#if defined(STELLAR_WIND) || defined(RESTRICT)
  int nl, nd;
#endif

#if defined(ION_RADIATION)
  Real flux;
#endif

  /* Constants */
  Ggrav = par_getd_def("constants", "Ggrav", 6.67259e-8);
  k_B   = par_getd_def("constants", "k_B", 1.380658e-16);
  m_HI  = par_getd_def("constants", "m_HI", 1.673528e-24);
  m_HII = par_getd_def("constants", "m_HII", 1.672621898e-24);

#if defined(STELLAR_WIND) || defined(ROTATING)
  /* User set stellar reference variables. Default: Solar at .05 AU */
  adist   = par_getd_def("problem", "adist", 7.48e11);
  Ms = par_getd_def("problem", "Ms", 1.99e33);
  GM_star = Ggrav*Ms;
  S_rmask = par_getd("problem", "S_rmask");
#if defined(STELLAR_WIND)
  S_rib   = par_getd("problem", "S_rib");
#endif
#if defined(ROTATING)
  /* Derived stellar quantities */
  Omega   = sqrt((GM_star+GM)/pow(adist, 3.));
  Rc = adist*Ms/(Ms+Mp);
  rhill   = adist*pow(Mp/(3.*Ms), 1./3.);
#if defined(SHEARING_BOX)
  Omega_0 = Omega;
  qshear  = 0.0;
  ShBoxCoord = xy;
  ShearingBoxPot = UnstratifiedDisk;
#endif
#endif
#endif

#if defined(PLANET)
  /* User set atmopshere reference variables. Default: Tripathi 2015 */
  r0 = par_getd_def("problem", "r0", 1.5e10);
  Mp = par_getd_def("problem", "Mp", 1.0e30);
  T0 = par_getd_def("problem", "T0", 1.1e3);

  /* Atmosphere boundaries */
  rmask = par_getd("problem", "rmask");
  rib   = par_getd("problem", "rib");

  /* Derived planet quantities */
  GM   = Ggrav*Mp;
  nrho = 1.0/Gamma_1;
  nprs = Gamma/Gamma_1;

  /* Set number density at rp such that tau=1 there */
  Rcrit  = Gamma_1*Ggrav*Mp*m_HI/(Gamma*k_B*T0*r0);
  Rz = Rcrit/(Rcrit-1.);
  beta   = tgamma(1.-nrho)*tgamma(1.+nrho)/tgamma(2.);
  Gscale = Rz*pow(Rz-1., -nrho)*beta
           *(1.-regularized_incomplete_beta(1./Rz, 1.-nrho, 1+nrho));

  sigma_pHI = par_getd_def("ionradiation", "sigma_pHI", 6.3e-18);
  n0   = 1./(Gscale*sigma_pHI*r0);
  rho0 = n0*m_HI;
  E0   = n0*k_B*T0/Gamma_1;
  phi0 = mech_potenial(-r0, 0.0, 0.0);

  phimask = mech_potenial(-rmask*r0, 0.0, 0.0);
  fmask   = 1.+rho0*(phi0-phimask)/(Gamma*E0);
  rhomask = rho0*pow(fmask, nrho);
  Emask   = E0*pow(fmask, nprs);
#endif

#if defined(RESTRICT) && !defined(STELLAR_WIND)
  rho2 = par_getd("problem", "rho2");
  E2   = par_getd("problem", "E2");
#endif

  /* Stellar wind parameters */
  n_star_wind   = par_getd_def("problem", "ns", 2.4e3);
  vel_star_wind = par_getd_def("problem", "vs", 2.5e7);
  T_star_wind   = par_getd_def("problem", "Ts", 1.5e6);
#ifdef MHD
  Real B_star_wind = par_getd_def("problem", "Bs", 1.0e-2);
#endif

  /* Derived stellar wind quantities */
  if ((stellar_wind_0 = (PrimS*)calloc_1d_array(1, sizeof(PrimS))) == NULL) {
    ath_error("[hse_planet]: Failed to allocate wind array\n");
  }
  stellar_wind_0->d  = n_star_wind*m_HII;
  stellar_wind_0->V1 = vel_star_wind;
  stellar_wind_0->V2 = 0.0;
  stellar_wind_0->V3 = 0.0;
#ifndef BAROTROPIC
  stellar_wind_0->P  = 2*n_star_wind*k_B*T_star_wind;
#endif
#ifdef MHD
  stellar_wind_0->B1c = B_star_wind;
  stellar_wind_0->Bc2 = 0.0;
  stellar_wind_0->B3c = 0.0;
#endif
#if (NSCALARS > 0)
  int s;
  for (s = 0; s < NSCALARS; s++) {
    switch (s) {
    case 0:   /* neutral H */
      stellar_wind_0->r[s] = 1.0e-10;
      break;
      ;
      ;
    case 1:   /* planetary gas */
      stellar_wind_0->r[s] = 0.0;
      break;
      ;
      ;
    default:
      stellar_wind_0->r[s] = -1.0;
      ;
      ;
    }
  }
#endif

  StaticGravPot = mech_potenial;

#if defined(STELLAR_WIND)
  /* Bernoulli constant of stellar wind */
  b_star_wind = 0.5*(SQR(stellar_wind_0->V1)+SQR(stellar_wind_0->V2)
                     +SQR(stellar_wind_0->V3));
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  b_star_wind -= GM_star/S_rmask;
#endif
#ifndef BAROTROPIC
  cs02_star_wind  = Gamma*stellar_wind_0->P/stellar_wind_0->d;
#else /* ISOTHERMAL */
  cs02_star_wind += 2.*k_B*T_star_wind/m_HII;
#endif
  b_star_wind += cs02_star_wind/Gamma_1;

  /* Boundary conditions */
  for (nl = 0; nl < (pM->NLevels); nl++) {
    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if ((pM->Domain[nl][nd]).Grid != NULL) {
        init_stellar_wind(&(pM->Domain[nl][nd]));
      }
    }
  }
#elif defined(RESTRICT)
  for (nl = 0; nl < (pM->NLevels); nl++) {
    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if ((pM->Domain[nl][nd]).Disp[0] == 0) {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x1, restrict_inflow_bc_ix1);
      }
      if ((pM->Domain[nl][nd]).MaxX[0] == pM->Domain[nl][nd].RootMaxX[0]) {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x1, restrict_inflow_bc_ox1);
      }
      if ((pM->Domain[nl][nd]).Disp[1] == 0) {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x2, restrict_inflow_bc_ix2);
      }
      if ((pM->Domain[nl][nd]).MaxX[1] == pM->Domain[nl][nd].RootMaxX[1]) {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x2, restrict_inflow_bc_ox2);
      }
      if ((pM->Domain[nl][nd]).Disp[2] == 0) {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), left_x3, restrict_inflow_bc_ix3);
      }
      if ((pM->Domain[nl][nd]).MaxX[2] == pM->Domain[nl][nd].RootMaxX[2]) {
        bvals_mhd_fun(&(pM->Domain[nl][nd]), right_x3, restrict_inflow_bc_ox3);
      }
    }
  }
#endif /* if defined(STELLAR_WIND) */

#if defined(ION_RADIATION)
  if (par_geti("ionradiation", "nradplanes") == 1) {
    flux = par_getd("ionradiation", "flux");
    for (nl = 0; nl < (pM->NLevels); nl++) {
      for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
        if ((pM->Domain[nl][nd]).Grid != NULL) {
          pM->Domain[nl][nd].Grid->radplanelist[0].dir  = -1;
          pM->Domain[nl][nd].Grid->radplanelist[0].flux = flux;
        }
      }
    }
  }
#endif

  return;
}

/*=============================================================================
 *! \fn ConsFun_t get_usr_expr(const char *expr)                              *
 *  \brief Set function pointers to desired user defined output               *
 *----------------------------------------------------------------------------*/

ConsFun_t get_usr_expr(const char *expr){
  if (strcmp(expr, "ionf") == 0) {
    return ionfrac;
  }
  if (strcmp(expr, "temp") == 0) {
    return gas_temp;
  }
#if defined(ION_RADPLANE) && !defined(WRITE_GHOST_CELLS)
  /* Radtrans didn't think of ghost zones output, needs upate if desired */
  if (strcmp(expr, "tau") == 0) {
    return plane_tau;
  }
  if (strcmp(expr, "flux") == 0) {
    return ion_flux;
  }
  if (strcmp(expr, "edot_HI_lya") == 0) {
    return edot_HI_lya;
  }
  if (strcmp(expr, "edot_HI_recomb") == 0) {
    return edot_HI_recomb;
  }
  if (strcmp(expr, "edot_HI_ion") == 0) {
    return edot_HI_ion;
  }
  if (strcmp(expr, "edot_pdV") == 0) {
    return edot_pdV;
  }
  if (strcmp(expr, "edot_advect") == 0) {
    return edot_advect;
  }
  if (strcmp(expr, "ndot_HI_recomb") == 0) {
    return ndot_HI_recomb;
  }
  if (strcmp(expr, "ndot_HI_ion") == 0) {
    return ndot_HI_ion;
  }
  if (strcmp(expr, "ndot_HI_advect") == 0) {
    return ndot_HI_advect;
  }
  if (strcmp(expr, "cell_dt") == 0) {
    return cell_dt;
  }
#endif
  return NULL;
}

/*=============================================================================
 *! \fn VOutFun_t get_usr_out_fun(const char *name)                           *
 *  \brief                                                                    *
 *----------------------------------------------------------------------------*/

VOutFun_t get_usr_out_fun(const char *name){
  return NULL;
}

/*=============================================================================
 *! \fn void Userwork_in_loop(MeshS *pM)                                      *
 *  \brief Work to be done in main loop. Used to mask and reset values in     *
 *         cells we wish to keep constant.                                    *
 *----------------------------------------------------------------------------*/

void Userwork_in_loop(MeshS *pM) {
  GridS *pG;
  int is, ie, js, je, ks, ke, nl, nd;
  int i, j, k;
  Real x1, x2, x3, rad;

#if defined(STELLAR_WIND)
  NewtonS fS;
  fS.f  = bernFunc;
  fS.df = dbernFunc;
  Real stellar_wind_vel, c_ph, s_ph, c_th, s_th;
#endif

#if defined(PLANET)
  Real frad, phi;
#endif

#if (NSCALARS > 0)
  int s;
#endif

  for (nl = 0; nl < (pM->NLevels); nl++) {
    for (nd = 0; nd < (pM->DomainsPerLevel[nl]); nd++) {
      if (pM->Domain[nl][nd].Grid != NULL) {
        pG = pM->Domain[nl][nd].Grid;

        is = pG->is;
        ie = pG->ie;
        js = pG->js;
        je = pG->je;
        ks = pG->ks;
        ke = pG->ke;

        for (k = ks; k <= ke; k++) {
          for (j = js; j <= je; j++) {
            for (i = is; i <= ie; i++) {
              cc_pos(pG, i, j, k, &x1, &x2, &x3);
              rad = sqrt(x1*x1+x2*x2+x3*x3);
#if defined(PLANET)
              phi = mech_potenial(x1, x2, x3);
#endif

              if ((pG->U[k][j][i].d < 0) || isnan(pG->U[k][j][i].d)) {
                ath_error("Neg or NaN dens: %e at |(%.02e, %.02e, %.02e)|=%.02e " \
                          "proc:%d, i:%d j:%d k:%d, lev:%d \n",
                          pG->U[k][j][i].d, x1, x2, x3, rad,
                          myID_Comm_world, i, j, k,
                          nl);
              }

              if (pG->U[k][j][i].E < 0 || isnan(pG->U[k][j][i].E)) {
                ath_error("Neg or NaN E: %e at |(%.02e, %.02e, %.02e)|=%.02e " \
                          "proc:%d, i:%d j:%d k:%d, lev:%d \n",
                          pG->U[k][j][i].E, x1, x2, x3, rad,
                          myID_Comm_world, i, j, k, nl);
              }

#if defined(PLANET)
              /*Reset values within the boundary*/
              if (rad <= rmask*r0) {
                pG->U[k][j][i].d   = rhomask;
                pG->U[k][j][i].M1  = 0.0;
                pG->U[k][j][i].M2  = 0.0;
                pG->U[k][j][i].M3  = 0.0;
                pG->U[k][j][i].E   = Emask;
#ifdef MHD
                pG->U[k][j][i].B1c = 0.0;
                pG->U[k][j][i].B2c = 0.0;
                pG->U[k][j][i].B3c = 0.0;
#endif
#if (NSCALARS > 0)
                for (s = 0; s < NSCALARS; s++) {
                  pG->U[k][j][i].s[s] = pG->U[k][j][i].d;
                }
#endif
              }
              else if (rad < rib*r0) {
                frad = 1.+rho0*(phi0-phi)/(Gamma*E0);
                pG->U[k][j][i].d   = rho0*pow(frad, nrho);
                pG->U[k][j][i].M1  = 0.0;
                pG->U[k][j][i].M2  = 0.0;
                pG->U[k][j][i].M3  = 0.0;
                pG->U[k][j][i].E   = E0*pow(frad, nprs);
#ifdef MHD
                pG->U[k][j][i].B1c = 0.0;
                pG->U[k][j][i].B2c = 0.0;
                pG->U[k][j][i].B3c = 0.0;
#endif
#if (NSCALARS > 0)
                for (s = 0; s < NSCALARS; s++) {
                  pG->U[k][j][i].s[s] = pG->U[k][j][i].d;
                }
#endif
              }
              else{
              }
#endif /* PLANET */
#if defined(STELLAR_WIND)
              /* Reset values within stellar boundary */
              r_star_wind = MAX(S_rmask, sqrt(SQR(x1+adist)+x2*x2+x3*x3));
              if (r_star_wind < S_rib) {
                c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
                s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
                c_th = x3/r_star_wind;
                s_th = sqrt(1-SQR(x3/r_star_wind));

                stellar_wind_vel = bisectnewt(fS,
                                              lobnd/
                                              pow(r_star_wind,
                                                  2.*Gamma_1/(Gamma+1.)),
                                              upbnd, 1.0);
                PrimS stellar_wind;
                stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
                stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
                stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
                stellar_wind.V1 += x2*Omega_0;
                stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
                stellar_wind.d   = stellar_wind_0->d
                                   *stellar_wind_0->V1/stellar_wind_vel
                                   *SQR(S_rmask/r_star_wind);
                stellar_wind.P = stellar_wind_0->P
                                 *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
                for (s = 0; s < NSCALARS; s++) {
                  stellar_wind.r[s] = stellar_wind_0->r[s];
                }
#endif

                pG->U[k][j][i] = Prim_to_Cons(&stellar_wind);
              }
              else{
              }
#endif /* STELLAR_WIND */
            }
          }
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn void Userwork_after_loop(MeshS *pM)                                   *
 *  \brief Work to do after simulation terminates. Currently none.            *
 *----------------------------------------------------------------------------*/

void Userwork_after_loop(MeshS *pM){
  return;
}

/*=============================================================================
 *=========================== PRIVATE FUNCTIONS ===============================
 *=============================================================================
 * Potential functions    - Sets the related potential                        *
 * Output functions       - User defined output                               *
 * Stellar wind functions - Related to solving the stellar wind               *
 * Boundary functions     - User defined boundaries                           *
 *----------------------------------------------------------------------------*/

/*========================== POTENTIAL FUNCTIONS =============================*/

/*=============================================================================
 *! \fn static Real mech_potenial(const Real x1, const Real x2, const Real x3)*
 *  \brief Setup the static potential field                                   *
 *----------------------------------------------------------------------------*/

static Real mech_potenial(const Real x1, const Real x2, const Real x3) {
  Real phi;

#if defined(PLANET)
  Real r_planet;
#endif
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  Real r_star, r2_perp;
#elif defined(STELLAR_WIND)
  Real r_star;
#endif

  phi = 0.0;

#if defined(PLANET)
  r_planet = sqrt(SQR(x1)+SQR(x2)+SQR(x3));
  r_planet = MAX(r_planet, rmask*r0/2.);
  phi -= GM/r_planet;
#endif
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  r_star  = sqrt(SQR(x1+adist)+SQR(x2)+SQR(x3));
  r_star  = MAX(r_star, S_rmask);
  r2_perp = SQR(x1+Rc)+SQR(x2);
  r2_perp = MAX(r2_perp, SQR(S_rmask));
  phi -= GM_star/r_star+0.5*SQR(Omega)*r2_perp;
#elif defined(STELLAR_WIND)
/* If no tidal forces no potential */
#endif

  return phi;
}

/*=============================================================================
 *! \fn static Real UnstratifiedDisk(const Real x1,                           *
 *                                   const Real x2, const Real x3)            *
 *  \brief Setup the effective potential field                                *
 *----------------------------------------------------------------------------*/

static Real UnstratifiedDisk(const Real x1, const Real x2, const Real x3)
{
  Real phi=0.0;
  return phi;
}

/*============================ OUTPUT FUNCTIONS ==============================*/

#if defined(ION_RADPLANE)
/*=============================================================================
 *! \fn static Real ionfrac(const GridS *pG,                                  *
 *                          const int i, const int j, const int k)            *
 *  \brief Calculate the ionization fraction of the cell                      *
 *----------------------------------------------------------------------------*/

static Real ionfrac(const GridS *pG, const int i, const int j, const int k) {
  return 1.-pG->U[k][j][i].s[0]/pG->U[k][j][i].d;
}

#if !defined(WRITE_GHOST_CELLS)
/* Sadly, ghost zones were not implemented to do radtransfer */

/*=============================================================================
 *! \fn static Real optical_depth(const GridS *pG,                            *
 *                                const int i, const int j, const int k)      *
 *  \brief Calculate the optical depth from a plane source                    *
 *--------------------------------------------------------------------------- */

static Real plane_tau(const GridS *pG, const int i, const int j, const int k) {
  Real flux, inboundFlux, exp_tau;

  flux = pG->radplanelist[0].CFlux[k][j][i];
  if (flux <= 0.0) {
    return 100.*log(10);
  }

  inboundFlux = pG->radplanelist[0].flux*pG->radplanelist[0].ramp;
  exp_tau = inboundFlux/flux;
  if (exp_tau >= 1.e100) {
    return 100.*log(10);
  }
  else {
    return log(exp_tau);
  }
}

/*=============================================================================
 *! \fn static Real ion_flux(const GridS *pG,                                 *
 *                                const int i, const int j, const int k)      *
 *  \brief Output the cell edge flux from a plane source                      *
 *--------------------------------------------------------------------------- */

static Real ion_flux(const GridS *pG, const int i, const int j, const int k){
  return pG->radplanelist[0].CFlux[k][j][i];
}

#endif /* !defined(WRITE_GHOST_CELLS) */

static Real edot_HI_lya(const GridS *pG, const int i, const int j,
                        const int k) {
  return pG->Dots[k][j][i].edot_HI_lya;
}

static Real edot_HI_recomb(const GridS *pG, const int i, const int j,
                           const int k) {
  return pG->Dots[k][j][i].edot_HI_recomb;
}

static Real edot_HI_ion(const GridS *pG, const int i, const int j,
                        const int k) {
  return pG->Dots[k][j][i].edot_HI_ion;
}

static Real edot_pdV(const GridS *pG, const int i, const int j, const int k){
  return pG->Dots[k][j][i].edot_pdV;
}

static Real edot_advect(const GridS *pG, const int i, const int j,
                        const int k) {
  return pG->Dots[k][j][i].edot_advect;
}

static Real ndot_HI_recomb(const GridS *pG, const int i, const int j,
                           const int k) {
  return pG->Dots[k][j][i].ndot_HI_recomb;
}

static Real ndot_HI_ion(const GridS *pG, const int i, const int j,
                        const int k) {
  return pG->Dots[k][j][i].ndot_HI_ion;
}

static Real ndot_HI_advect(const GridS *pG, const int i, const int j,
                           const int k) {
  return pG->Dots[k][j][i].ndot_HI_advect;
}

static Real cell_dt(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].dt;
}

#endif /* defined(ION_RADPLANE) */

/*=============================================================================
 *! \fn static Real gas_temp(const GridS *pG,                                 *
 *                           const int i, const int j, const int k)           *
 *  \brief Calculate the gas temperature of the cell                          *
 *--------------------------------------------------------------------------- */

static Real gas_temp(const GridS *pG, const int i, const int j,
                     const int k){
  Real k_B, alpha_C, e_thermal;
  Real n_H, n_Hplus, n_e, n_tot;

  k_B = par_getd("constants", "k_B");
  alpha_C = par_getd("ionradiation", "alpha_C");

  n_H     = pG->U[k][j][i].s[0]/m_HI;
  n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_HII;
  n_e     = n_Hplus+pG->U[k][j][i].d*alpha_C/(14.0*m_HI);
  n_tot   = n_e+n_H+n_Hplus;

  /* Get gas temperature in K */
  e_thermal = pG->U[k][j][i].E
              -0.5*(pG->U[k][j][i].M1*pG->U[k][j][i].M1+
                    pG->U[k][j][i].M2*pG->U[k][j][i].M2+
                    pG->U[k][j][i].M3*pG->U[k][j][i].M3)
              /pG->U[k][j][i].d;
#ifdef MHD
  e_thermal -= 0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
                    pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
                    pG->U[k][j][i].B3c*pG->U[k][j][i].B3c);
#endif
  return Gamma_1/(n_tot*k_B)*e_thermal;
}

#if defined(STELLAR_WIND)
/*========================= STELLAR WIND FUNCTIONS ===========================*/

/*=============================================================================
 *! \fn void init_stellar_wind(DomainS *pD)                                   *
 *  \brief Calculates stellar wind array, and determines if stellar wind      *
 *         boundary conditions are necessary based on inflow conditions.      *
 *         If user boundaries are desired set here, but will be superceded    *
 *         by stellar boundaries, feel free to change if desired.             *
 *--------------------------------------------------------------------------- */

static void init_stellar_wind(DomainS *pD){
  GridS *pG = pD->Grid;
  int i, max_i;

#if (NSCALARS > 0)
  int s;
#endif

  /* Write 3d BC arrays, assume computation limited */
  int il, iu;
  int j, jl, ju;
  int k, kl, ku;
  int ie = pG->ie, je = pG->je, ke = pG->ke;

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = ie+nghost;
  }
  else {
    il = pG->is;
    iu = ie;
  }

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = je+nghost;
  }
  else {
    jl = pG->js;
    ju = je;
  }

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = ke;
  }

  if (pG->MinX[0] == pD->RootMinX[0]) {
    cons_stellar_wind_ix1 = (ConsS***)calloc_3d_array(ku-kl+1, ju-jl+1,
                                                      nghost, sizeof(ConsS));
    stellar_wind_ix1 = (PrimS***)calloc_3d_array(ku-kl+1, ju-jl+1,
                                                 nghost, sizeof(PrimS));
  }
  if (pG->MaxX[0] == pD->RootMaxX[0]) {
    cons_stellar_wind_ox1 = (ConsS***)calloc_3d_array(ku-kl+1, ju-jl+1,
                                                      nghost, sizeof(ConsS));
    stellar_wind_ox1 = (PrimS***)calloc_3d_array(ku-kl+1, ju-jl+1,
                                                 nghost, sizeof(PrimS));
  }
  if (pG->MinX[1] == pD->RootMinX[1]) {
    cons_stellar_wind_ix2 = (ConsS***)calloc_3d_array(ku-kl+1, nghost,
                                                      iu-il+1, sizeof(ConsS));
    stellar_wind_ix2 = (PrimS***)calloc_3d_array(ku-kl+1, nghost,
                                                 iu-il+1, sizeof(PrimS));
  }
  if (pG->MaxX[1] == pD->RootMaxX[1]) {
    cons_stellar_wind_ox2 = (ConsS***)calloc_3d_array(ku-kl+1, nghost,
                                                      iu-il+1, sizeof(ConsS));
    stellar_wind_ox2 = (PrimS***)calloc_3d_array(ku-kl+1, nghost,
                                                 iu-il+1, sizeof(PrimS));
  }
  if (pG->MinX[2] == pD->RootMinX[2]) {
    cons_stellar_wind_ix3 = (ConsS***)calloc_3d_array(nghost, ju-jl+1,
                                                      iu-il+1, sizeof(ConsS));
    stellar_wind_ix3 = (PrimS***)calloc_3d_array(nghost, ju-jl+1,
                                                 iu-il+1, sizeof(PrimS));
  }
  if (pG->MaxX[2] == pD->RootMaxX[2]) {
    cons_stellar_wind_ox3 = (ConsS***)calloc_3d_array(nghost, ju-jl+1,
                                                      iu-il+1, sizeof(ConsS));
    stellar_wind_ox3 = (PrimS***)calloc_3d_array(nghost, ju-jl+1,
                                                 iu-il+1, sizeof(PrimS));
  }

  PrimS stellar_wind;
  int stellar_ix1_bc_flag = 0, stellar_ix2_bc_flag = 0, stellar_ix3_bc_flag = 0;
  int stellar_ox1_bc_flag = 0, stellar_ox2_bc_flag = 0, stellar_ox3_bc_flag = 0;
  Real stellar_wind_vel;
  Real x1, x2, x3, rad;
  Real c_ph, s_ph, c_th, s_th;

  /* Setup Newton-Rapshen structure for root finding in our problem */
  NewtonS fS;

  /* Calculate maximum velocity of wind, set as upper bound for root finding */
  fS.f  = maxFunc;
  fS.df = dmaxFunc;

  /* Assume wind does not accelerate over million times intial value */
  upbnd = bisectnewt(fS, stellar_wind_0->V1, 1.e6*stellar_wind_0->V1, 1.0);
  lobnd = pow(cs02_star_wind*pow(stellar_wind_0->V1*SQR(S_rmask), Gamma_1),
              1./(Gamma+1.));

  /* Switch functions to bernoulli equation to solve for u */
  fS.f  = bernFunc;
  fS.df = dbernFunc;

  /* Using cubic spline fits, given we know slopes and values at all points */
  if (pG->MinX[0] == pD->RootMinX[0] || pG->MaxX[0] == pD->RootMaxX[0] ||
      pG->MinX[1] == pD->RootMinX[1] || pG->MaxX[1] == pD->RootMaxX[1]) {
    for (k = kl; k <= ku; k++) {
      if (pG->MinX[0] == pD->RootMinX[0] || pG->MaxX[0] == pD->RootMaxX[0]) {
        for (j = jl; j <= ju; j++) {
          if (pG->MinX[0] == pD->RootMinX[0]) {
            for (i = 0; i < nghost; i++) {
              cc_pos(pG, i, j, k, &x1, &x2, &x3);
              r_star_wind = MAX(S_rmask, sqrt(SQR(adist+x1)+SQR(x2)+SQR(x3)));
              c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
              s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
              c_th = x3/r_star_wind;
              s_th = sqrt(1-SQR(x3/r_star_wind));

              stellar_wind_vel = bisectnewt(fS,
                                            lobnd/
                                            pow(r_star_wind,
                                                2.*Gamma_1/(Gamma+1.)), upbnd,
                                            1.0);
              PrimS stellar_wind;
              stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
              stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
              stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
              stellar_wind.V1 += x2*Omega_0;
              stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
              stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/
                                 stellar_wind_vel
                                 *SQR(S_rmask/r_star_wind);
              stellar_wind.P = stellar_wind_0->P
                               *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
              for (s = 0; s < NSCALARS; s++) {
                stellar_wind.r[s] = stellar_wind_0->r[s];
              }
#endif

              stellar_wind_ix1[k][j][i] = stellar_wind;
              if (stellar_ix1_bc_flag == 0 &&
                  stellar_wind_ix1[k][j][i].V1 > 0) {
                bvals_mhd_fun(pD, left_x1, stellar_wind_bc_ix1);
                stellar_ix1_bc_flag = 1;
              }
              cons_stellar_wind_ix1[k][j][i] = Prim_to_Cons(&stellar_wind);
              pG->U[k][j][i] = cons_stellar_wind_ix1[k][j][i];
            }
          }
          if (pG->MaxX[0] == pD->RootMaxX[0]) {
            for (i = 0; i < nghost; i++) {
              cc_pos(pG, ie+1+i, j, k, &x1, &x2, &x3);
              r_star_wind = MAX(S_rmask, sqrt(SQR(adist+x1)+SQR(x2)+SQR(x3)));
              c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
              s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
              c_th = x3/r_star_wind;
              s_th = sqrt(1-SQR(x3/r_star_wind));

              stellar_wind_vel = bisectnewt(fS,
                                            lobnd/
                                            pow(r_star_wind,
                                                2.*Gamma_1/(Gamma+1.)), upbnd,
                                            1.0);
              PrimS stellar_wind;
              stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
              stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
              stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
              stellar_wind.V1 += x2*Omega_0;
              stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
              stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/
                                 stellar_wind_vel
                                 *SQR(S_rmask/r_star_wind);
              stellar_wind.P = stellar_wind_0->P
                               *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
              for (s = 0; s < NSCALARS; s++) {
                stellar_wind.r[s] = stellar_wind_0->r[s];
              }
#endif

              stellar_wind_ox1[k][j][i] = stellar_wind;
              if (stellar_ox1_bc_flag == 0 &&
                  stellar_wind_ox1[k][j][i].V1 < 0) {
                bvals_mhd_fun(pD, right_x1, stellar_wind_bc_ox1);
                stellar_ox1_bc_flag = 1;
              }
              cons_stellar_wind_ox1[k][j][i] = Prim_to_Cons(&stellar_wind);
              pG->U[k][j][ie+1+i] = cons_stellar_wind_ox1[k][j][i];
            }
          }
        }
      }
      if (pG->MinX[1] == pD->RootMinX[1] || pG->MaxX[1] == pD->RootMaxX[1]) {
        for (i = il; i <= iu; i++) {
          if (pG->MinX[1] == pD->RootMinX[1]) {
            for (j = 0; j < nghost; j++) {
              cc_pos(pG, i, j, k, &x1, &x2, &x3);
              r_star_wind = MAX(S_rmask, sqrt(SQR(adist+x1)+SQR(x2)+SQR(x3)));
              c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
              s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
              c_th = x3/r_star_wind;
              s_th = sqrt(1-SQR(x3/r_star_wind));

              stellar_wind_vel = bisectnewt(fS,
                                            lobnd/
                                            pow(r_star_wind,
                                                2.*Gamma_1/(Gamma+1.)), upbnd,
                                            1.0);
              PrimS stellar_wind;
              stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
              stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
              stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
              stellar_wind.V1 += x2*Omega_0;
              stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
              stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/
                                 stellar_wind_vel
                                 *SQR(S_rmask/r_star_wind);
              stellar_wind.P = stellar_wind_0->P
                               *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
              for (s = 0; s < NSCALARS; s++) {
                stellar_wind.r[s] = stellar_wind_0->r[s];
              }
#endif

              stellar_wind_ix2[k][j][i] = stellar_wind;
              if (stellar_ix2_bc_flag == 0 &&
                  stellar_wind_ix2[k][j][i].V2 > 0) {
                bvals_mhd_fun(pD, left_x2, stellar_wind_bc_ix2);
                stellar_ix2_bc_flag = 1;
              }
              cons_stellar_wind_ix2[k][j][i] = Prim_to_Cons(&stellar_wind);
              pG->U[k][j][i] = cons_stellar_wind_ix2[k][j][i];
            }
          }
          if (pG->MaxX[1] == pD->RootMaxX[1]) {
            for (j = 0; j < nghost; j++) {
              cc_pos(pG, i, je+1+j, k, &x1, &x2, &x3);
              r_star_wind = MAX(S_rmask, sqrt(SQR(adist+x1)+SQR(x2)+SQR(x3)));
              c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
              s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
              c_th = x3/r_star_wind;
              s_th = sqrt(1-SQR(x3/r_star_wind));

              stellar_wind_vel = bisectnewt(fS,
                                            lobnd/
                                            pow(r_star_wind,
                                                2.*Gamma_1/(Gamma+1.)), upbnd,
                                            1.0);
              PrimS stellar_wind;
              stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
              stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
              stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
              stellar_wind.V1 += x2*Omega_0;
              stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
              stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/
                                 stellar_wind_vel
                                 *SQR(S_rmask/r_star_wind);
              stellar_wind.P = stellar_wind_0->P
                               *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
              for (s = 0; s < NSCALARS; s++) {
                stellar_wind.r[s] = stellar_wind_0->r[s];
              }
#endif

              stellar_wind_ox2[k][j][i] = stellar_wind;
              if (stellar_ox2_bc_flag == 0 &&
                  stellar_wind_ox2[k][j][i].V2 < 0) {
                bvals_mhd_fun(pD, right_x2, stellar_wind_bc_ox2);
                stellar_ox2_bc_flag = 1;
              }
              cons_stellar_wind_ox2[k][j][i] = Prim_to_Cons(&stellar_wind);
              pG->U[k][je+1+j][i] = cons_stellar_wind_ox2[k][j][i];
            }
          }
        }
      }
    }
  }
  if (pG->MinX[2] == pD->RootMinX[2] || pG->MaxX[2] == pD->RootMaxX[2]) {
    for (i = il; i <= iu; i++) {
      for (j = jl; j <= ju; j++) {
        if (pG->MinX[2] == pD->RootMinX[2]) {
          for (k = 0; k < nghost; k++) {
            cc_pos(pG, i, j, k, &x1, &x2, &x3);
            r_star_wind = MAX(S_rmask, sqrt(SQR(adist+x1)+SQR(x2)+SQR(x3)));
            c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
            s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
            c_th = x3/r_star_wind;
            s_th = sqrt(1-SQR(x3/r_star_wind));

            stellar_wind_vel = bisectnewt(fS,
                                          lobnd/
                                          pow(r_star_wind,
                                              2.*Gamma_1/(Gamma+1.)), upbnd,
                                          1.0);
            PrimS stellar_wind;
            stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
            stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
            stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
            stellar_wind.V1 += x2*Omega_0;
            stellar_wind.V2 -= (Rc+x1)*Omega_0;
              stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
            stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/
                               stellar_wind_vel
                               *SQR(S_rmask/r_star_wind);
            stellar_wind.P = stellar_wind_0->P
                             *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
            for (s = 0; s < NSCALARS; s++) {
              stellar_wind.r[s] = stellar_wind_0->r[s];
            }
#endif
            stellar_wind_ix3[k][j][i] = stellar_wind;
            if (stellar_ix3_bc_flag == 0 && stellar_wind_ix3[k][j][i].V3 > 0) {
              bvals_mhd_fun(pD, left_x3, stellar_wind_bc_ix3);
              stellar_ix3_bc_flag = 1;
            }
            cons_stellar_wind_ix3[k][j][i] = Prim_to_Cons(&stellar_wind);
            pG->U[k][j][i] = cons_stellar_wind_ix3[k][j][i];
          }
        }
        if (pG->MaxX[2] == pD->RootMaxX[2]) {
          for (k = 0; k < nghost; k++) {
            cc_pos(pG, i, j, ke+1+k, &x1, &x2, &x3);
            r_star_wind = MAX(S_rmask, sqrt(SQR(adist+x1)+SQR(x2)+SQR(x3)));
            c_ph = (x1+adist)/sqrt(SQR(x1+adist)+SQR(x2));
            s_ph = x2/sqrt(SQR(x1+adist)+SQR(x2));
            c_th = x3/r_star_wind;
            s_th = sqrt(1-SQR(x3/r_star_wind));

            stellar_wind_vel = bisectnewt(fS,
                                          lobnd/
                                          pow(r_star_wind,
                                              2.*Gamma_1/(Gamma+1.)), upbnd,
                                          1.0);
            PrimS stellar_wind;
            stellar_wind.V1  = stellar_wind_vel*c_ph*s_th;
            stellar_wind.V2  = stellar_wind_vel*s_ph*s_th;
            stellar_wind.V3  = stellar_wind_vel*c_th;
#if defined(CORIOLIS) || defined(SHEARING_BOX)
            stellar_wind.V1 += x2*Omega_0;
            stellar_wind.V2 -= (Rc+x1)*Omega_0;
#endif
            stellar_wind.d   = stellar_wind_0->d*stellar_wind_0->V1/
                               stellar_wind_vel
                               *SQR(S_rmask/r_star_wind);
            stellar_wind.P = stellar_wind_0->P
                             *pow(stellar_wind.d/stellar_wind_0->d, Gamma);
#if (NSCALARS > 0)
            for (s = 0; s < NSCALARS; s++) {
              stellar_wind.r[s] = stellar_wind_0->r[s];
            }
#endif
            stellar_wind_ox3[k][j][i] = stellar_wind;
            if (stellar_ox3_bc_flag == 0 && stellar_wind_ox3[k][j][i].V3 < 0) {
              bvals_mhd_fun(pD, right_x3, stellar_wind_bc_ox3);
              stellar_ox3_bc_flag = 1;
            }
            cons_stellar_wind_ox3[k][j][i] = Prim_to_Cons(&stellar_wind);
            pG->U[ke+1+k][j][i] = cons_stellar_wind_ox3[k][j][i];
          }
        }
      }
    }
  }

  /* Other user defined boundary conditions, superseded by stellar boundary */
  if (stellar_ix1_bc_flag == 0 && pG->MinX[0] == pD->RootMinX[0]) {
#if defined(RESTRICT)
    bvals_mhd_fun(pD, left_x1, restrict_inflow_bc_ix1);
#endif
  }
  if (stellar_ox1_bc_flag == 0 && pG->MaxX[0] == pD->RootMaxX[0]) {
#if defined(RESTRICT)
    bvals_mhd_fun(pD, right_x1, restrict_inflow_bc_ox1);
#endif
  }
  if (stellar_ix2_bc_flag == 0 && pG->MinX[1] == pD->RootMinX[1]) {
#if defined(RESTRICT)
    bvals_mhd_fun(pD, left_x1, restrict_inflow_bc_ix2);
#endif
  }
  if (stellar_ox2_bc_flag == 0 && pG->MaxX[1] == pD->RootMaxX[1]) {
#if defined(RESTRICT)
    bvals_mhd_fun(pD, right_x1, restrict_inflow_bc_ox2);
#endif
  }
  if (stellar_ix3_bc_flag == 0 && pG->MinX[2] == pD->RootMinX[2]) {
#if defined(RESTRICT)
    bvals_mhd_fun(pD, left_x1, restrict_inflow_bc_ix3);
#endif
  }
  if (stellar_ox3_bc_flag == 0 && pG->MaxX[2] == pD->RootMaxX[2]) {
#if defined(RESTRICT)
    bvals_mhd_fun(pD, right_x1, restrict_inflow_bc_ox3);
#endif
  }

  return;
}

/*=============================================================================
 *! \fn static Real bernFunc(const Real u, const Real r, const Real b,        *
 *             const Real GM, const Real r0, const Real u0, const Real cs02)  *
 *  \brief The bernoulli equation for a spherically symmetric point potential *
 *--------------------------------------------------------------------------- */

static Real bernFunc(const Real vel){
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  return 0.5*SQR(vel)+cs02_star_wind/Gamma_1
         *pow(stellar_wind_0->V1*SQR(S_rmask)/(vel*SQR(r_star_wind)), Gamma_1)
         -(GM_star/r_star_wind+b_star_wind);
#else
  return 0.5*SQR(vel)+cs02_star_wind/Gamma_1
         *pow(stellar_wind_0->V1*SQR(S_rmask)/(vel*SQR(r_star_wind)), Gamma_1)
         -(b_star_wind);
#endif
}

/*=============================================================================
 *! \fn static Real dbernFunc(const Real u, const Real r, const Real r0,      *
 *                            const Real u0, const Real cs02, const Real GM)  *
 *  \brief The derivative of a spherically symmetric bernoulli equation       *
 *--------------------------------------------------------------------------- */

static Real dbernFunc(const Real vel){
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  return vel-cs02_star_wind/vel
         *pow(stellar_wind_0->V1*SQR(S_rmask)/(vel*SQR(S_rmask)), Gamma_1);
#else
  return vel-cs02_star_wind/vel
         *pow(stellar_wind_0->V1*SQR(S_rmask)/(vel*SQR(S_rmask)), Gamma_1);
#endif
}

/*=============================================================================
 *! \fn static Real maxFunc(const Real u, const Real r, const Real b,         *
 *       const Real GM, const Real r0, const Real u0, const Real cs02)        *
 *  \brief The bernoulli equation with r_max, a function of u, substituted for*
 *         r, thus u solved for is u_max.                                     *
 *--------------------------------------------------------------------------- */

static Real maxFunc(const Real vel){
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  return 0.5*SQR(vel)+cs02_star_wind/Gamma_1
         *pow(vel/(stellar_wind_0->V1*SQR(S_rmask)), Gamma_1/(2.*Gamma-3.))
         *pow(GM_star/(2.*cs02_star_wind), (2.-2.*Gamma)/(3.-2.*Gamma))
         -b_star_wind-GM_star/
         pow(GM_star/(2.*cs02_star_wind)*
             pow(vel/(stellar_wind_0->V1*SQR(S_rmask)), Gamma_1),
             1./(3.-2.*Gamma));
#else
  return 0.5*SQR(vel)-b_star_wind;
#endif
}

/*=============================================================================
 *! \fn static Real dmaxFunc(const Real u, const Real r, const Real r0,       *
 *                            const Real u0, const Real cs02, const Real GM)  *
 *  \brief The derivative of r_max bernoulli equation                         *
 *--------------------------------------------------------------------------- */

static Real dmaxFunc(const Real vel){
#if defined(CORIOLIS) || defined(SHEARING_BOX) || defined(TIDAL_FORCES)
  return vel+2.*cs02_star_wind/(2.*Gamma-3.)*pow(vel, 1./(2.*Gamma-3.))*
         pow(GM_star/(2.*cs02_star_wind)*
             pow(1./(stellar_wind_0->V1*SQR(S_rmask)), Gamma_1),
             (2.*Gamma-2.)/(2*Gamma-3.));
#else
  return vel;
#endif
}

/*========================== BOUNDARY FUNCTIONS ==============================*/

/*=============================================================================
 *! \fn static void stellar_wind_bc_ix1(GridS *pG)                            *
 *  \brief Boundary condition fixed to inflow the stellar wind solution,      *
 *         Doesn't set outflow to stellar, lets domain set that.              *
 *         If the total pressure in the domain excedes that of the stellar    *
 *         wind, allow to outflow locally.                                    *
 *--------------------------------------------------------------------------- */

static void stellar_wind_bc_ix1(GridS *pG){
  int i, is = pG->is;
  int j, jl, ju;
  int k, kl, ku;

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  for (k = kl; k <= ku; k++) {
    for (j = jl; j <= ju; j++) {
      PrimS Wtmp;
      Wtmp = Cons_to_Prim(&(pG->U[k][j][is]));
      if (Wtmp.V1 < 0 &&
          (Wtmp.P+0.5*Wtmp.d*SQR(Wtmp.V1) >
           stellar_wind_ix1[k][j][is-1].P+
           0.5*SQR(stellar_wind_ix1[k][j][is-1].V1)*
           stellar_wind_ix1[k][j][is-1].d)
          ) {
        for (i = 0; i < nghost; i++) {
          pG->U[k][j][i] = pG->U[k][j][is];
        }
      }
      else{
        for (i = 0; i < nghost; i++) {
          pG->U[k][j][i] = cons_stellar_wind_ix1[k][j][i];
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn static void stellar_wind_bc_ix2(GridS *pG)                            *
 *  \brief Boundary condition fixed to inflow the stellar wind solution,      *
 *         Doesn't set outflow to stellar, lets domain set that.              *
 *         If the total pressure in the domain excedes that of the stellar    *
 *         wind, allow to outflow locally.                                    *
 *--------------------------------------------------------------------------- */

static void stellar_wind_bc_ix2(GridS *pG){
  int i, il, iu;
  int j, js = pG->js;
  int k, kl, ku;

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  for (k = kl; k <= ku; k++) {
    for (i = il; i <= iu; i++) {
      PrimS Wtmp;
      Wtmp = Cons_to_Prim(&(pG->U[k][js][i]));
      if (Wtmp.V2 < 0 &&
          (Wtmp.P+0.5*Wtmp.d*SQR(Wtmp.V2) >
           stellar_wind_ix2[k][js-1][i].P+
           0.5*SQR(stellar_wind_ix2[k][js-1][i].V2)*
           stellar_wind_ix2[k][js-1][i].d)
          ) {
        for (j = 0; j < nghost; j++) {
          pG->U[k][j][i] = pG->U[k][js][i];
        }
      }
      else{
        for (j = 0; j < nghost; j++) {
          pG->U[k][j][i] = cons_stellar_wind_ix2[k][j][i];
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn static void stellar_wind_bc_ix3(GridS *pG)                            *
 *  \brief Boundary condition fixed to inflow the stellar wind solution,      *
 *         Doesn't set outflow to stellar, lets domain set that.              *
 *         If the total pressure in the domain excedes that of the stellar    *
 *         wind, allow to outflow locally.                                    *
 *--------------------------------------------------------------------------- */

static void stellar_wind_bc_ix3(GridS *pG){
  int i, il, iu;
  int j, jl, ju;
  int k, ks = pG->ks;

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  for (j = jl; j <= ju; j++) {
    for (i = il; i <= iu; i++) {
      PrimS Wtmp;
      Wtmp = Cons_to_Prim(&(pG->U[ks][j][i]));
      if (Wtmp.V3 < 0 &&
          (Wtmp.P+0.5*Wtmp.d*SQR(Wtmp.V3) >
           stellar_wind_ix3[ks-1][j][i].P+
           0.5*SQR(stellar_wind_ix3[ks-1][j][i].V3)*
           stellar_wind_ix3[ks-1][j][i].d)
          ) {
        for (k = 0; k < nghost; k++) {
          pG->U[k][j][i] = pG->U[ks][j][i];
        }
      }
      else{
        for (k = 0; k < nghost; k++) {
          pG->U[k][j][i] = cons_stellar_wind_ix3[k][j][i];
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn static void stellar_wind_bc_ox1(GridS *pG)                            *
 *  \brief Boundary condition fixed to inflow the stellar wind solution,      *
 *         Doesn't set outflow to stellar, lets domain set that.              *
 *         If the total pressure in the domain excedes that of the stellar    *
 *         wind, allow to outflow locally.                                    *
 *--------------------------------------------------------------------------- */

static void stellar_wind_bc_ox1(GridS *pG){
  int i, ie = pG->ie;
  int j, jl, ju;
  int k, kl, ku;

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  for (k = kl; k <= ku; k++) {
    for (j = jl; j <= ju; j++) {
      PrimS Wtmp;
      Wtmp = Cons_to_Prim(&(pG->U[k][j][ie]));
      if (Wtmp.V1 > 0 &&
          (Wtmp.P+0.5*Wtmp.d*SQR(Wtmp.V1) >
           stellar_wind_ox1[k][j][0].P+
           0.5*SQR(stellar_wind_ox1[k][j][0].V1)*stellar_wind_ox1[k][j][0].d)
          ) {
        for (i = 0; i < nghost; i++) {
          pG->U[k][j][ie+1+i] = pG->U[k][j][ie];
        }
      }
      else{
        for (i = 0; i < nghost; i++) {
          pG->U[k][j][ie+1+i] = cons_stellar_wind_ox1[k][j][i];
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn static void stellar_wind_bc_ox2(GridS *pG)                            *
 *  \brief Boundary condition fixed to inflow the stellar wind solution,      *
 *         Doesn't set outflow to stellar, lets domain set that.              *
 *         If the total pressure in the domain excedes that of the stellar    *
 *         wind, allow to outflow locally.                                    *
 *--------------------------------------------------------------------------- */

static void stellar_wind_bc_ox2(GridS *pG){
  int i, il, iu;
  int j, je = pG->je;
  int k, kl, ku;

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  for (k = kl; k <= ku; k++) {
    for (i = il; i <= iu; i++) {
      PrimS Wtmp;
      Wtmp = Cons_to_Prim(&(pG->U[k][je][i]));
      if (Wtmp.V2 > 0 &&
          (Wtmp.P+0.5*Wtmp.d*SQR(Wtmp.V2) >
           stellar_wind_ox2[k][0][i].P+
           0.5*SQR(stellar_wind_ox2[k][0][i].V2)*stellar_wind_ox2[k][0][i].d)
          ) {
        for (j = 0; j < nghost; j++) {
          pG->U[k][je+1+j][i] = pG->U[k][je][i];
        }
      }
      else{
        for (j = 0; j < nghost; j++) {
          pG->U[k][je+1+j][i] = cons_stellar_wind_ox2[k][j][i];
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn static void stellar_wind_bc_ox3(GridS *pG)                            *
 *  \brief Boundary condition fixed to inflow the stellar wind solution,      *
 *         Doesn't set outflow to stellar, lets domain set that.              *
 *         If the total pressure in the domain excedes that of the stellar    *
 *         wind, allow to outflow locally.                                    *
 *--------------------------------------------------------------------------- */

static void stellar_wind_bc_ox3(GridS *pG){
  int i, il, iu;
  int j, jl, ju;
  int k, ke = pG->ke;

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  for (j = jl; j <= ju; j++) {
    for (i = il; i <= iu; i++) {
      PrimS Wtmp;
      Wtmp = Cons_to_Prim(&(pG->U[ke][j][i]));
      if (Wtmp.V3 > 0 &&
          (Wtmp.P+0.5*Wtmp.d*SQR(Wtmp.V3) >
           stellar_wind_ox3[0][j][i].P+
           0.5*SQR(stellar_wind_ox3[0][j][i].V3)*stellar_wind_ox3[0][j][i].d)
          ) {
        for (k = 0; k < nghost; k++) {
          pG->U[ke+1+k][j][i] = pG->U[ke][j][i];
        }
      }
      else{
        for (k = 0; k < nghost; k++) {
          pG->U[ke+1+k][j][i] = cons_stellar_wind_ox3[k][j][i];
        }
      }
    }
  }

  return;
}

#endif /* defined(STELLAR_WIND) */
#if defined(RESTRICT)
/*=============================================================================
 *! \fn static Real restrict_inflow_bc_ox1(GridS *pG)                         *
 *  \brief Apply boundary condition in right-x (right) direction              *
 *         Don't alter outflow, but restrict inflow to ambient gas            *
 *----------------------------------------------------------------------------*/

static void restrict_inflow_bc_ox1(GridS *pG){
  int i, ie = pG->ie;
  int k, kl, ku;
  int j, jl, ju;

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else{
    kl = pG->ks;
    ku = pG->ke;
  }

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else{
    jl = pG->js;
    ju = pG->je;
  }

  for (j = jl; j <= ju; j++) {
    for (k = kl; k <= ku; k++) {
      if (pG->U[k][j][ie].M1 <= 0) {
        for (i = 0; i < nghost; i++) {
          PrimS Wtmp;
          Wtmp    = Cons_to_Prim(&(pG->U[k][j][ie]));
#if defined(STELLAR_WIND)
          Wtmp.d  = stellar_wind_ox1[k][j][i].d;
          Wtmp.P  = stellar_wind_ox1[k][j][i].P;
#else
          Wtmp.d  = rho2;
          Wtmp.V1 = 0.0;
          Wtmp.V2 = 0.0;
          Wtmp.V3 = 0.0;
          Wtmp.P  = Gamma_1*E2;
#endif
          pG->U[k][j][ie+1+i] = Prim_to_Cons(&Wtmp);
        }
      }
      else {
        if (pG->U[k][j][ie].d != pG->U[k][j][ie].d ||
            pG->U[k][j][ie].M1 != pG->U[k][j][ie].M1 ||
            pG->U[k][j][ie].M2 != pG->U[k][j][ie].M2 ||
            pG->U[k][j][ie].M3 != pG->U[k][j][ie].M3 ||
            pG->U[k][j][ie].E != pG->U[k][j][ie].E ||
            pG->U[k][j][ie].s[0] != pG->U[k][j][ie].s[0] ||
            pG->U[k][j][ie].s[1] != pG->U[k][j][ie].s[1]
            ) {
          ath_pout(-1,
                   "rank:%d||(%d,%d)||d=%e||M1=%e||M2=%e||M3=%e||E=%e||s[0]=%e||s[1]=%e.\n", myID_Comm_world, j, k,
                   pG->U[k][j][ie+1+i].d,
                   pG->U[k][j][ie].M1, pG->U[k][j][ie].M2, pG->U[k][j][ie].M3,
                   pG->U[k][j][ie].E, pG->U[k][j][ie].s[0],
                   pG->U[k][j][ie].s[1]);
        }
        for (i = 0; i < nghost; i++) {
          pG->U[k][j][ie+1+i] = pG->U[k][j][ie];
        }
      }
    }
  }

  return;
}

/*=============================================================================
 *! \fn static Real restrict_inflow_bc_ox2GridS *pG)                          *
 *  \brief Apply boundary condition in right-y (up) direction                 *
 *         Don't alter outflow, but restrict inflow to ambient gas            *
 *----------------------------------------------------------------------------*/

static void restrict_inflow_bc_ox2(GridS *pG){
  int j, je = pG->je;
  int k, kl, ku;
  int i, il, iu;

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  for (i = il; i <= iu; i++) {
    for (k = kl; k <= ku; k++) {
      if (pG->U[k][je][i].M2 <= 0) {
        for (j = 0; j < nghost; j++) {
          PrimS Wtmp;
          Wtmp    = Cons_to_Prim(&(pG->U[k][je][i]));
#if defined(STELLAR_WIND)
          Wtmp.d  = stellar_wind_ox2[k][j][i].d;
          Wtmp.P  = stellar_wind_ox2[k][j][i].P;
#else
          Wtmp.d  = rho2;
          Wtmp.V1 = 0.0;
          Wtmp.V2 = 0.0;
          Wtmp.V3 = 0.0;
          Wtmp.P  = Gamma_1*E2;
#endif
          pG->U[k][je+1+j][i] = Prim_to_Cons(&Wtmp);
        }
      }
      else {
        for (j = 0; j < nghost; j++) {
          pG->U[k][je+1+j][i] = pG->U[k][je][i];
        }
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real restrict_inflow_bc_ox3(GridS *pG)                         *
 *  \brief Apply boundary condition in right-z (out) direction                *
 *         Don't alter outflow, but restrict inflow to maximally suck in      *
 *         ambient gas                                                        */

static void restrict_inflow_bc_ox3(GridS *pG){
  int k, ke = pG->ke;
  int j, jl, ju;
  int i, il, iu;

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  for (i = il; i <= iu; i++) {
    for (j = jl; j <= ju; j++) {
      if (pG->U[ke][j][i].M3 <= 0) {
        for (k = 0; k < nghost; k++) {
          PrimS Wtmp;
          Wtmp    = Cons_to_Prim(&(pG->U[ke][j][i]));
#if defined(STELLAR_WIND)
          Wtmp.d  = stellar_wind_ox3[k][j][i].d;
          Wtmp.P  = stellar_wind_ox3[k][j][i].P;
#else
          Wtmp.d  = rho2;
          Wtmp.V1 = 0.0;
          Wtmp.V2 = 0.0;
          Wtmp.V3 = 0.0;
          Wtmp.P  = Gamma_1*E2;
#endif
          pG->U[ke+1+k][j][i] = Prim_to_Cons(&Wtmp);
        }
      }
      else {
        for (k = 0; k < nghost; k++) {
          pG->U[ke+1+k][j][i] = pG->U[ke][j][i];
        }
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real restrict_inflow_bc_ix1(GridS *pG)                         *
 *  \brief Apply boundary condition in left-x (left) direction                *
 *         Don't alter outflow, but restrict inflow to maximally suck in      *
 *         ambient gas                                                        */

static void restrict_inflow_bc_ix1(GridS *pG){
  int i, is = pG->is;
  int k, kl, ku;
  int j, jl, ju;

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  for (j = jl; j <= ju; j++) {
    for (k = kl; k <= ku; k++) {
      if (pG->U[k][j][is].M1 >= 0) {
        for (i = 0; i < nghost; i++) {
          PrimS Wtmp;
          Wtmp    = Cons_to_Prim(&(pG->U[k][j][is]));
#if defined(STELLAR_WIND)
          Wtmp.d  = stellar_wind_ix1[k][j][i].d;
          Wtmp.P  = stellar_wind_ix1[k][j][i].P;
#else
          Wtmp.d  = rho2;
          Wtmp.V1 = 0.0;
          Wtmp.V2 = 0.0;
          Wtmp.V3 = 0.0;
          Wtmp.P  = Gamma_1*E2;
#endif
          pG->U[k][j][i] = Prim_to_Cons(&Wtmp);
        }
      }
      else {
        for (i = 0; i < nghost; i++) {
          pG->U[k][j][i] = pG->U[k][j][is];
        }
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real restrict_inflow_bc_ix2(GridS *pG)                         *
 *  \brief Apply boundary condition in left-y (down) direction                *
 *         Don't alter outflow, but restrict inflow to maximally suck in      *
 *         ambient gas                                                        */

static void restrict_inflow_bc_ix2(GridS *pG){
  int j, js = pG->js;
  int k, kl, ku;
  int i, il, iu;

  if (pG->Nx[2] > 1) {
    kl = pG->ks-nghost;
    ku = pG->ke+nghost;
  }
  else {
    kl = pG->ks;
    ku = pG->ke;
  }

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  for (i = il; i <= iu; i++) {
    for (k = kl; k <= ku; k++) {
      if (pG->U[k][js][i].M2 >= 0) {
        for (j = 0; j < nghost; j++) {
          PrimS Wtmp;
          Wtmp    = Cons_to_Prim(&(pG->U[k][js][i]));
#if defined(STELLAR_WIND)
          Wtmp.d  = stellar_wind_ix2[k][j][i].d;
          Wtmp.P  = stellar_wind_ix2[k][j][i].P;
#else
          Wtmp.d  = rho2;
          Wtmp.V1 = 0.0;
          Wtmp.V2 = 0.0;
          Wtmp.V3 = 0.0;
          Wtmp.P  = Gamma_1*E2;
#endif
          pG->U[k][j][i] = Prim_to_Cons(&Wtmp);
        }
      }
      else {
        for (j = 0; j < nghost; j++) {
          pG->U[k][j][i] = pG->U[k][js][i];
        }
      }
    }
  }

  return;
}

/*----------------------------------------------------------------------------*/
/*! \fn static Real restrict_inflow_bc_ix3(GridS *pG)                         *
 *  \brief Apply boundary condition in left-z (in) direction                  *
 *         Don't alter outflow, but restrict inflow to maximally suck in      *
 *         ambient gas                                                        */

static void restrict_inflow_bc_ix3(GridS *pG){
  int k, ks = pG->ks;
  int j, jl, ju;
  int i, il, iu;

  if (pG->Nx[1] > 1) {
    jl = pG->js-nghost;
    ju = pG->je+nghost;
  }
  else {
    jl = pG->js;
    ju = pG->je;
  }

  if (pG->Nx[0] > 1) {
    il = pG->is-nghost;
    iu = pG->ie+nghost;
  }
  else {
    il = pG->is;
    iu = pG->ie;
  }

  for (i = il; i <= iu; i++) {
    for (j = jl; j <= ju; j++) {
      if (pG->U[ks][j][i].M3 >= 0) {
        for (k = 0; k < nghost; k++) {
          PrimS Wtmp;
          Wtmp    = Cons_to_Prim(&(pG->U[ks][j][i]));
#if defined(STELLAR_WIND)
          Wtmp.d  = stellar_wind_ix3[k][j][i].d;
          Wtmp.P  = stellar_wind_ix3[k][j][i].P;
#else
          Wtmp.d  = rho2;
          Wtmp.V1 = 0.0;
          Wtmp.V2 = 0.0;
          Wtmp.V3 = 0.0;
          Wtmp.P  = Gamma_1*E2;
#endif
          pG->U[k][j][i] = Prim_to_Cons(&Wtmp);
        }
      }
      else {
        for (k = 0; k < nghost; k++) {
          pG->U[k][j][i] = pG->U[ks][j][i];
        }
      }
    }
  }

  return;
}

#endif /* defined(RESTRICT) */

#define NITR 20000
#define EPSILON 1.e-8

static Real incomplete_beta(Real x, Real a, Real b){
  return tgamma(a)*tgamma(b)/tgamma(a+b)*regularized_incomplete_beta(x, a, b);
}

static Real regularized_incomplete_beta(Real x, Real a, Real b){
  int i;
  Real cfrac, c, d, dit, ii, delta;
  Real coeff, apb, ap1, am1;

  if (x < 0. || x > 1.) {
    ath_error("[regularized_incomplete_beta]: x outside range.");
  }

  if (x == 0. || x == 1.) {
    return x;
  }

  /* Coefficient of the continued fraction */
  coeff = exp(a*log(x)+b*log(1.-x))*tgamma(a+b)/(tgamma(a)*tgamma(b));

  if (a <= 0) {
    return regularized_incomplete_beta(x, a+1, b)+coeff/a;
  }
  else if (a == 1) {
    return 1.-pow(1.-x, b);
  }

  if (b <= 0) {
    return regularized_incomplete_beta(x, a, b+1)-coeff/b;
  }
  else if (b == 1) {
    pow(x, a);
  }

  apb = a+b;
  ap1 = a+1.;

  /* Continued fraction converges in O(sqrt(min(a,b))) iterations for
   * x < (a+1)/(a+b+2)). If x is greater than use the symmetry relation */
  if (x > ap1/(apb+2.)) {
    return 1.-regularized_incomplete_beta(1.-x, b, a);
  }

  am1 = a-1.;

  c   = 1.;
  d   = 1.-apb*x/ap1;
  if (fabs(d) < TINY_NUMBER) {
    d = TINY_NUMBER;
  }
  d = 1./d;
  cfrac = d;

  /* Lentz's method for continued fraction */
  for (i = 1; i <= NITR; i++) {
    ii  = 2.*i;
    dit = (i*(b-i)*x)/((am1+ii)*(a+ii));

    d   = 1.+dit*d;
    c   = 1.+dit/c;

    if (fabs(d) < TINY_NUMBER) {
      d = TINY_NUMBER;
    }
    if (fabs(c) < TINY_NUMBER) {
      c = TINY_NUMBER;
    }
    d = 1./d;
    cfrac *= c*d;

    dit    = -((a+i)*(apb+i)*x)/((a+ii)*(ap1+ii));

    d = 1.+dit*d;
    c = 1.+dit/c;

    if (fabs(d) < TINY_NUMBER) {
      d = TINY_NUMBER;
    }
    if (fabs(c) < TINY_NUMBER) {
      c = TINY_NUMBER;
    }

    d = 1./d;

    const long double delta = c*d;
    cfrac *= delta;

    /* Check for convergence */
    if (fabs(1.-delta) <= EPSILON) {
      break;
    }
  }

  if (i == NITR) {
    ath_error(
      "[regularized_incomplete_beta]: Did not converge, impliment new methods.");
  }

  return coeff*cfrac/a;
}

#undef NITR
#undef EPS

static Real bisectnewt(NewtonS fS, Real xlo, Real xhi, const Real eps){
  int i;
  const int i_max = 40;
  Real f, df, root, droot, droot_old;
  Real flo = fS.f(xlo);
  Real fup = fS.f(xhi);

  if (flo*fup >= 0) {
    if (flo == 0) {
      return xlo;
    }
    else if (fup == 0) {
      return xhi;
    }
    ath_error("Bracketed none or even number of roots, require single root\n");
  }
  if (flo > 0) {
    Real swap = xlo;
    xlo = xhi;
    xhi = swap;
  }

  root  = 0.5*(xhi+xlo);
  droot_old = fabs(xhi-xlo);
  droot = droot_old;
  f = fS.f(root);
  if (f == 0.0) {
    return root;
  }
  df = fS.df(root);
  for (i = 0; i < i_max; i++) {
    if ((f+(xhi-root)*df)*(f-(root-xlo)*df) > 0 ||  /* Out of range */
        fabs(2.*f) > fabs(df*droot_old) /* slow convergence */) { /* Bisect */
      droot_old = droot;
      droot = 0.5*(xhi-xlo);
      root  = xlo+droot;
    }
    else{ /* Newton-Raphson */
      droot_old = droot;
      droot = f/df;
      root -= droot;
    }
    if (fabs(droot) < eps) {
      return root;
    }

    f = fS.f(root);
    if (f == 0.0) {
      return root;
    }
    df = fS.df(root);
    if (f < 0.0) {
      xlo = root;
    }
    else{
      xhi = root;
    }
  }

  ath_error("exceeded maximum converge iterations: %e, %e, %e\n", xhi, xlo,
            root);
  return 0.0;
}
