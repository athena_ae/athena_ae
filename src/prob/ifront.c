/*
 * Function ifront.c
 *
 * Problem generator for ionization front propagating from domain edge
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

#define TII 1.0e4
#define ALPHA4 2.59e-13

/*----------------------------------------------------------------------------*
*======================== PRIVATE FUNCTION PROTOTYPES =======================*
*----------------------------------------------------------------------------*/

/*========================== POTENTIAL FUNCTIONS =============================*/
/*============================ OUTPUT FUNCTIONS ==============================*/
static Real ionfrac(const GridS *pG, const int i, const int j, const int k);
static Real gas_temp(const GridS *pG, const int i, const int j, const int k);
static Real plane_tau(const GridS *pG, const int i, const int j, const int k);
static Real ion_flux(const GridS *pG, const int i, const int j, const int k);
static Real edot_HI_lya(const GridS *pG, const int i, const int j, const int k);
/*
static Real edot_HI_recomb(const GridS *pG, const int i, const int j, const int k);
static Real edot_HI_ion(const GridS *pG, const int i, const int j, const int k);
static Real edot_pdV(const GridS *pG, const int i, const int j, const int k);
static Real edot_advect(const GridS *pG, const int i, const int j, const int k);
static Real ndot_HI_recomb(const GridS *pG, const int i, const int j, const int k);
static Real ndot_HI_ion(const GridS *pG, const int i, const int j, const int k);
static Real ndot_HI_advect(const GridS *pG, const int i, const int j, const int k);
*/

void problem(DomainS *pD){
  GridS *pG = pD->Grid;
  int i, is = pG->is, ie = pG->ie;
  int j, js = pG->js, je = pG->je;
  int k, ks = pG->ks, ke = pG->ke;
  int radplanecount = 0;
  Real cs, n_H, m_H, flux;
  Real rho, pressure;

#ifdef MHD
  Real bx, by, bz;
#endif

/* Set up uniform ambinet medium with ionizing source at its
 * edge. The user-input parameters are n_H (initial density),
 * cs (initial sound speed in neutral gas), flux (ionizing flux,
 * in units of photons per unit time per unit area), and bx, by, and
 * bz (initial vector magnetic field).
 */
  m_H  = par_getd("ionradiation", "m_H");
  n_H  = par_getd("problem", "n_H");
  cs   = par_getd("problem", "cs");
  flux = par_getd("problem", "flux");
#ifdef MHD
  bx   = par_getd("problem", "Bx");
  by   = par_getd("problem", "By");
  bz   = par_getd("problem", "Bz");
#endif

  /* Power-law pressure and density */
  for (k = ks; k <= ke+1; k++) {
    for (j = js; j <= je+1; j++) {
      for (i = is; i <= ie+1; i++) {
        rho = n_H*m_H;
        pressure = rho*cs*cs;
        pG->U[k][j][i].d    = rho;
        pG->U[k][j][i].M1   = 0.0;
        pG->U[k][j][i].M2   = 0.0;
        pG->U[k][j][i].M3   = 0.0;
        pG->U[k][j][i].E    = pressure/Gamma_1;
        pG->U[k][j][i].s[0] = rho;
#ifdef MHD
        pG->U[k][j][i].B1c  = bx;
        pG->U[k][j][i].B2c  = by;
        pG->U[k][j][i].B3c  = bz;
        pG->B1i[k][j][i]    = bx;
        pG->B2i[k][j][i]    = by;
        pG->B3i[k][j][i]    = bz;
        pG->U[k][j][i].E   += (bx*bx+by*by+bz*bz)/2.0;
#endif
      }
    }
  }

  if (radplanecount == par_geti("problem", "nradplanes")) {
    ath_error("Invalid number of radplanes specified in input file\n");
  }
  if (radplanecount != par_geti("problem", "nradplanes")) {
    add_radplane_3d(pD, -1, flux);
    radplanecount++;
    if (radplanecount != par_geti("problem", "nradplanes")) {
      ath_error("Invalid number of radplanes specified in input file\n");
    }
  }

  return;
}

void Userwork_in_loop(MeshS *pM){
}

void Userwork_after_loop(MeshS *pM){
}

void problem_write_restart(MeshS *pM, FILE *fp){
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp){
  return;
}

ConsFun_t get_usr_expr(const char *expr){
  if (strcmp(expr, "ionf") == 0) {
    return ionfrac;
  }
  if (strcmp(expr, "temp") == 0) {
    return gas_temp;
  }
#if defined(ION_RADPLANE)
  if (strcmp(expr, "tau") == 0) {
    return plane_tau;
  }
  if (strcmp(expr, "flux") == 0) {
    return ion_flux;
  }
/*
  if(strcmp(expr,"edot_HI_lya")==0) return edot_HI_lya;
  if(strcmp(expr,"edot_HI_recomb")==0) return edot_HI_recomb;
  if(strcmp(expr,"edot_HI_ion")==0) return edot_HI_ion;
  if(strcmp(expr,"edot_pdV")==0) return edot_pdV;
  if(strcmp(expr,"edot_advect")==0) return edot_advect;
  if(strcmp(expr,"ndot_HI_recomb")==0) return ndot_HI_recomb;
  if(strcmp(expr,"ndot_HI_ion")==0) return ndot_HI_ion;
  if(strcmp(expr,"ndot_HI_advect")==0) return ndot_HI_advect;
*/
#endif
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name){
  return NULL;
}

/*============================ OUTPUT FUNCTIONS ==============================*/

/*=============================================================================
 *! \fn static Real ionfrac(const GridS *pG,                                  *
 *                          const int i, const int j, const int k)            *
 *  \brief Calculate the ionization fraction of the cell                      *
 *----------------------------------------------------------------------------*/

static Real ionfrac(const GridS *pG, const int i, const int j, const int k) {
  return 1.-pG->U[k][j][i].s[0]/pG->U[k][j][i].d;
}

#if defined(ION_RADPLANE)
/*=============================================================================
 *! \fn static Real optical_depth(const GridS *pG,                            *
 *                                const int i, const int j, const int k)      *
 *  \brief Calculate the optical depth from a plane source                    *
 *--------------------------------------------------------------------------- */

static Real plane_tau(const GridS *pG, const int i, const int j, const int k) {
  Real flux, inboundFlux, exp_tau;
  int n = 0;

  flux = pG->radplanelist[n].CFlux[k][j][i];
  if (flux <= 0.0) {
    return 100.*log(10);
  }

  inboundFlux = pG->radplanelist[n].flux;
  exp_tau = inboundFlux/flux;
  if (exp_tau >= 1.e100) {
    return 100.*log(10);
  }
  else {
    return log(exp_tau);
  }
}

/*=============================================================================
 *! \fn static Real ion_flux(const GridS *pG,                                 *
 *                                const int i, const int j, const int k)      *
 *  \brief Output the cell edge flux from a plane source                      *
 *--------------------------------------------------------------------------- */

static Real ion_flux(const GridS *pG, const int i, const int j, const int k) {
  int n = 0;

  return pG->radplanelist[n].CFlux[k][j][i];
}
/*
static Real edot_HI_lya(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].edot_HI_lya;
}

static Real edot_HI_recomb(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].edot_HI_recomb;
}

static Real edot_HI_ion(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].edot_HI_ion;
}

static Real edot_pdV(const GridS *pG, const int i, const int j, const int k){
  return pG->Dots[k][j][i].edot_pdV;
}

static Real edot_advect(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].edot_advect;
}

static Real ndot_HI_recomb(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].ndot_HI_recomb;
}

static Real ndot_HI_ion(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].ndot_HI_ion;
}

static Real ndot_HI_advect(const GridS *pG, const int i, const int j, const int k) {
  return pG->Dots[k][j][i].ndot_HI_advect;
}
*/
#endif

/*=============================================================================
 *! \fn static Real gas_temp(const GridS *pGrid,                              *
 *                           const int i, const int j, const int k)           *
 *  \brief Calculate the gas temperature of the cell                          *
 *--------------------------------------------------------------------------- */

static Real gas_temp(const GridS *pG, const int i, const int j, const int k){
  Real m_H, k_B, alpha_C, E_th, n_H, n_Hplus, n_e, n_tot;

  m_H = par_getd("ionradiation", "m_H");
  k_B = par_getd("ionradiation", "k_B");
  alpha_C = par_getd("ionradiation", "alpha_C");

  n_H     = pG->U[k][j][i].s[0]/m_H;
  n_Hplus = (pG->U[k][j][i].d-pG->U[k][j][i].s[0])/m_H;
  n_e     = n_Hplus+pG->U[k][j][i].d*alpha_C/(14.0*m_H);
  n_tot   = n_e+n_H+n_Hplus;

  /* Get gas temperature in K */
  E_th = pG->U[k][j][i].E
         -0.5*(pG->U[k][j][i].M1*pG->U[k][j][i].M1+
               pG->U[k][j][i].M2*pG->U[k][j][i].M2+
               pG->U[k][j][i].M3*pG->U[k][j][i].M3)
         /pG->U[k][j][i].d;
#ifdef MHD
  E_th -= 0.5*(pG->U[k][j][i].B1c*pG->U[k][j][i].B1c+
               pG->U[k][j][i].B2c*pG->U[k][j][i].B2c+
               pG->U[k][j][i].B3c*pG->U[k][j][i].B3c);
#endif

  return Gamma_1/(n_tot*k_B)*E_th;
}
