/*
 * Function ifront.c
 *
 * Problem generator for ionization front propagating from domain edge
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include "athena.h"
#include "globals.h"
#include "prototypes.h"

#define TII 1.0e4
#define ALPHA4 2.59e-13

void problem(DomainS *pD){
  GridS *pG = pD->Grid;
  int i, is = pG->is, ie = pG->ie;
  int j, js = pG->js, je = pG->je;
  int k, ks = pG->ks, ke = pG->ke;
  int radplanecount = 0;
  Real cs, n_H, m_H, flux;
  Real rho, pressure;

#ifdef MHD
  Real bx, by, bz;
#endif

/* Set up uniform ambinet medium with ionizing source at its
 * edge. The user-input parameters are n_H (initial density),
 * cs (initial sound speed in neutral gas), flux (ionizing flux,
 * in units of photons per unit time per unit area), and bx, by, and
 * bz (initial vector magnetic field).
 */
  m_H  = par_getd("ionradiation", "m_H");
  n_H  = par_getd("problem", "n_H");
  cs   = par_getd("problem", "cs");
  flux = par_getd("problem", "flux");
#ifdef MHD
  bx   = par_getd("problem", "Bx");
  by   = par_getd("problem", "By");
  bz   = par_getd("problem", "Bz");
#endif

  /* Power-law pressure and density */
  for (k = ks; k <= ke+1; k++) {
    for (j = js; j <= je+1; j++) {
      for (i = is; i <= ie+1; i++) {
        rho = n_H*m_H;
        pressure = rho*cs*cs;
        pG->U[k][j][i].d    = rho;
        pG->U[k][j][i].M1   = 0.0;
        pG->U[k][j][i].M2   = 0.0;
        pG->U[k][j][i].M3   = 0.0;
        pG->U[k][j][i].E    = pressure/Gamma_1;
        pG->U[k][j][i].s[0] = rho;
#ifdef MHD
        pG->U[k][j][i].B1c  = bx;
        pG->U[k][j][i].B2c  = by;
        pG->U[k][j][i].B3c  = bz;
        pG->B1i[k][j][i]    = bx;
        pG->B2i[k][j][i]    = by;
        pG->B3i[k][j][i]    = bz;
        pG->U[k][j][i].E   += (bx*bx+by*by+bz*bz)/2.0;
#endif
      }
    }
  }

  if (radplanecount == par_geti("problem", "nradplanes")) {
    ath_error("Invalid number of radplanes specified in input file\n");
  }
  if (radplanecount != par_geti("problem", "nradplanes")) {
    add_radplane_3d(pD, -3, flux);
    radplanecount++;
    if (radplanecount != par_geti("problem", "nradplanes")) {
      ath_error("Invalid number of radplanes specified in input file\n");
    }
  }

  return;
}

void Userwork_in_loop(MeshS *pM){
}

void Userwork_after_loop(MeshS *pM){
}

void problem_write_restart(MeshS *pM, FILE *fp){
  return;
}

void problem_read_restart(MeshS *pM, FILE *fp){
  return;
}

ConsFun_t get_usr_expr(const char *expr){
  return NULL;
}

VOutFun_t get_usr_out_fun(const char *name){
  return NULL;
}
